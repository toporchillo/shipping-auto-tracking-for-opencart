﻿"Трек-номер заказа deliveryCRM" дополнение для OpenCart (ocStore)
автор Александр Топорков (toporchillo@gmail.com)

Модуль для OpenCart 1.5.x

Модуль позволяет хранить в заказе трек-номер заказа, и выполнять некоторые действия при его добавлении:
смена статуса заказа, уведомление покупателя по почте и SMS, экспорт трек-номера  заказа в сервис отслеживания
deliveryCRM (deliverycrm.ru)

УСТАНОВКА
---------
1. Убедитесь, что у вас установлен vQmod.

2. Скопируйте содержимое папки upload в файловую систему сайта поверх структуры папок.

3. Установка и настройка модуля доступна в системе администрирования, на странице
   "Дополнения - Доставка - DeliveryCRM. Трек-номер заказа"
  
4. Если модуль в системе администрирования не обнаруживается - убедитесь, что у пользователя есть
   права на просмотр и редактирование модуля shipping/deliverycrm

Если установка выполнена, то на странице просмотра заказа в блоке "Операции" появится поле ввода
для редактирования трек-номера заказа.
