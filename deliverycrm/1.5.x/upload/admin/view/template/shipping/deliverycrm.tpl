<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
              
          <tr>
            <td width="200">Статус:</td>
            <td>
              <select name="deliverycrm_status" id="input-status" class="form-control">			
                <?php if ($deliverycrm_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
		  
		  
          <tr>
              <td colspan="2"><h3>Что делать при добавлении трек-номера к заказу:</h3></td>
		  </tr>
		  
          <tr>
            <td>Менять статус заказа:</td>
            <td>
                <label><input class="_form_flag" rel="_change_status" type="checkbox" name="deliverycrm_change_status" value="1" <?php echo ($deliverycrm_change_status ? ' checked="checked"' : ''); ?>/></label>
            </td>
		  </tr>
              
              
          <tr class=" _change_status">
            <td>Устанавливать статус:</label>
			<td>
			  <select name="deliverycrm_order_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"<?php echo ($order_status['order_status_id'] == $deliverycrm_order_status ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
				<?php } ?>
              </select>
			</td>
		  </tr>
		  
          <tr>
            <td>Уведомлять покупателя по E-mail:</td>
			<td>
                <label><input class="_form_flag" rel="_email_notify" type="checkbox" name="deliverycrm_email_notify" value="1" <?php echo ($deliverycrm_email_notify ? ' checked="checked"' : ''); ?>/></label>
            </td>
          </tr>
		  
		  <tr class="_email_notify">
			<td>Текст письма:</td>
			<td>
				<textarea name="deliverycrm_email_text" rows="10" cols="100" class="form-control"><?php echo $deliverycrm_email_text; ?></textarea>
                <br><span class="help">Подстановка в письмо: {order_id} - номер заказа, {track_no} - трек-номер, {carrier_name} - служба доставки, {shipping_firstname} и {shipping_lastname} - имя и фамилия покупателя.</span>
			</td>
		  </tr>

		  <tr>
			<td>Уведомлять покупателя по SMS:</td>
            <td><label><input class="_form_flag" rel="_sms_notify" type="checkbox" name="deliverycrm_sms_notify" value="1" <?php echo ($deliverycrm_sms_notify ? ' checked="checked"' : ''); ?>/></label>
				<?php if (!$sms_on) { ?><span style="color: red;" data-toggle="tooltip" title="Перейдите в раздел Система - Настройки - Мой магазин, и там включите SMS-отправление">SMS-информирование у вас отключено, либо не поддерживается. SMS отправляться не будут.</span><?php } ?>
            </td>
		  </tr>
		  
		  <tr class="_sms_notify">
			<td>Текст SMS:</td>
			<td>
				<textarea name="deliverycrm_sms_text" rows="5" cols="100" class="form-control"><?php echo $deliverycrm_sms_text; ?></textarea>
                <br><span class="help">Подстановка в SMS: {order_id} - номер заказа, {track_no} - трек-номер, {carrier_name} - служба доставки, {shipping_firstname} и {shipping_lastname} - имя и фамилия покупателя.</span>
			</td>
		  </tr>

		  <tr>
			<td>Экспортировать в <a href="http://deliverycrm.ru" target="_blank">DeliveryCRM</a>:</td>
			<td>
				<label><input class="_form_flag" rel="_export_deliverycrm" type="checkbox" name="deliverycrm_export" value="1" <?php echo ($deliverycrm_export ? ' checked="checked"' : ''); ?>/>
				</label>
			</td>
		  </tr>

		  <tr class="_export_deliverycrm">
			<td>ID пользователя:</td>
			<td>
				<input type="text" name="deliverycrm_user" value="<?php echo $deliverycrm_user; ?>" class="form-control input-small" />
				<a href="https://deliverycrm.ru/client/profile/index" target="_blank">получить ID пользователя</a>
			</td>
		  </tr>
      
		  <tr class="_export_deliverycrm">
			<td>Ключ API:</td>
			<td>
				<input type="text" name="deliverycrm_api_id" value="<?php echo $deliverycrm_api_id; ?>" class="form-control input-small" />
				<a href="https://deliverycrm.ru/client/profile/index" target="_blank">получить ключ API</a>
			</td>
		  </tr>

		  <tr class="_export_deliverycrm">
			<td>Тестовый режим:</td>
			<td>
              <select name="deliverycrm_test_mode" class="form-control">			
                <?php if ($deliverycrm_test_mode == '1') { ?>
                <option value="1" selected="selected">Да</option>
                <option value="0">Нет</option>
                <?php } else { ?>
                <option value="1">Да</option>
                <option value="0" selected="selected">Нет</option>
                <?php } ?>
              </select>
			</td>
		  </tr>
              
		  <tr class="_export_deliverycrm">
            <td>Используемые перевозчики:</td>
            <td>
              <select name="deliverycrm_carriers[]" id="deliverycrm-carriers" size="17" multiple class="form-control">			
				<?php foreach ($all_carriers as $num=>$carrier) { ?>
				<option value="<?php echo $num; ?>"<?php echo (isset($deliverycrm_carriers[$num]) ? ' selected="selected"' : ''); ?>><?php echo $carrier; ?></option>
				<?php } ?>
              </select>
            </td>
		  </tr>
		</table>
			
		<input type="hidden" name="deliverycrm_set" value="1" />
      </form>
  </div>
</div>
<script>
$(document).ready(function() {

$('._form_flag').change(function() {
	if ($(this).is(':checked')) {
		$('.'+$(this).attr('rel')).show();
	}
	else {
		$('.'+$(this).attr('rel')).hide();
	}
});
$('._form_flag').trigger('change');


})
</script>
<?php echo $footer; ?>