<?php
/**
* Order Track Number for OpenCart (ocStore) 1.5.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*/
class ControllerShippingTrackNo extends Controller {
	protected $error = array();
	public $LOG_LEVEL = 4;
	
	public $CONFIG = array(
		'change_status'=>1,
		'order_status'=>2,
		//'order_history'=>'Заказ отправлен, трек-номер: {track_no}.',
		
		'email_notify'=>1,
		'email_text'=>'Уважаемый {shipping_firstname} {shipping_lastname}, ваш заказ №{order_id} передан службе доставки, трек-номер: {track_no}.',

		'sms_notify'=>0,
		'sms_text'=>'Ваш заказ №{order_id} передан службе доставки, трек-номер: {track_no}.',
		
		'export_liveinform'=>0,
		'liveinform_api_id'=>'',
		'liveinform_sync'=>0,
		'shipping_status'=>2,
		'postoffice_status'=>2,
		'issued_status'=>3,
		'return_status'=>8,
		'sync_comment'=>'Статус доставки: "{operation}, {operation_text}"; Почтовое отделение: "{geo}, {index}"'
	);

	private function setConfig() {
		if ($this->config->get('track_no_set')) {
			foreach($this->CONFIG as $key=>$conf) {
				$this->CONFIG[$key] = $this->config->get('track_no_'.$key);
			}
		}
	}
	
	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='track_no'");
	}
	
	public function index() {
		$this->load->language('shipping/track_no');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['track_no_order_statuses'] = isset($this->request->post['track_no_order_statuses']) ? implode(',', $this->request->post['track_no_order_statuses']) : '';
			$this->model_setting_setting->editSetting('track_no', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
			return;
		}
		
		$this->data['liveinform_sync_url'] = HTTP_SERVER.'liveinform_updater.php';
		
		$this->setConfig();
		if (isset($this->request->post['track_no_order_statuses'])) {
			$this->data['track_no_order_statuses'] = $this->request->post['track_no_order_statuses'];
		} else {
			$this->data['track_no_order_statuses'] = explode(',', $this->config->get('track_no_order_statuses'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/track_no', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/track_no', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 		
		
		
		$this->load->model('localisation/order_status');
    	$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		if (isset($this->request->post['track_no_status'])) {
			$this->data['track_no_status'] = $this->request->post['track_no_status'];
		} else {
			$this->data['track_no_status'] = $this->config->get('track_no_status');
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['track_no_'.$key])) {
				$this->data['track_no_'.$key] = $this->request->post['track_no_'.$key];
			} else {
				$this->data['track_no_'.$key] = $conf;
			}
		}

		$this->data['sms_on'] = $this->config->get('config_sms_alert');
		
		$this->data['store'] = HTTPS_CATALOG;
		$this->data['token'] = $this->session->data['token'];

		$this->data['text_loading'] = $this->language->get('text_loading');
		$this->data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
		$this->data['button_ip_add'] = $this->language->get('button_ip_add');

		$this->template = 'shipping/track_no.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/track_no')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		else if (isset($this->request->post['track_no_export_liveinform']) && $this->request->post['track_no_export_liveinform'] && !$this->request->post['track_no_liveinform_api_id']) {
			$this->error['warning'] = 'Вы выбрали экспортировать заказы в LiveInform.ru, но не указали API ID.';
		}
		else if (isset($this->request->post['track_no_liveinform_sync']) && !isset($this->request->post['track_no_order_statuses'])) {
			$this->error['warning'] = 'Укажите один или несколько статусов заказа, которые не надо отслеживать.';
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
    
    
	public function save() {
		$this->load->language('sale/order');

		$json = array();

		$this->load->model('shipping/track_no');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
			$saved = $this->model_shipping_track_no->save($order_id, $this->request->post['track_no']);
			if (!$saved) {
				$json['error'] = 'Трек-номер (идентификатор отправления) не сохранен. Или модуль отключен, или вы сохраняете прежний трек-номер.';
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				return;
			}
			$json['success'] = 'Трек-номер (идентификатор отправления) сохранен!';
			if ($this->model_shipping_track_no->liveinform_error) {
				$json['error'] = 'LiveInform не принял трек-номер для отслеживания: '.$this->model_shipping_track_no->liveinform_error;
			}
			elseif ($this->model_shipping_track_no->liveinform_success) {
				$json['success'].= ' Трек-номер добавлен для отслеживания в LiveInform.';
			}
		} else {
			$order_id = 0;
			$json['error'] = $this->language->get('error_not_found');
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    
    public function update_liveinform() {
		if (isset($_SERVER['HTTP_HOST'])) {
			header("Content-Type: text/html; charset=utf-8");
		}
		if (!$this->config->get('track_no_liveinform_sync')) {
			echo 'Синхронизация заказов с LiveInform отключена. Включить можно в настройках модуля "Трек-номер заказа"';
		}
		else {
			$this->load->model('shipping/track_no');
			echo "Синхронизация заказов с LiveInform...<br/>\n";
			$orders = $this->model_shipping_track_no->updateLiveinform();
			echo "Синхронизация завершена\n";
		}
    }
    
}
