<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
              
          <tr>
            <td width="200">Статус:</td>
            <td>
              <select name="track_no_status" id="input-status" class="form-control">			
                <?php if ($track_no_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
		  
		  
          <tr>
              <td colspan="2"><h3>Что делать при добавлении трек-номера к заказу:</h3></td>
		  </tr>
		  
          <tr>
            <td>Менять статус заказа:</td>
            <td>
                <label><input class="_form_flag" rel="_change_status" type="checkbox" name="track_no_change_status" value="1" <?php echo ($track_no_change_status ? ' checked="checked"' : ''); ?>/></label>
            </td>
		  </tr>
              
              
          <tr class=" _change_status">
            <td>Устанавливать статус:</label>
			<td>
			  <select name="track_no_order_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"<?php echo ($order_status['order_status_id'] == $track_no_order_status ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
				<?php } ?>
              </select>
			</td>
		  </tr>
		  
          <tr>
            <td>Уведомлять покупателя по E-mail:</td>
			<td>
                <label><input class="_form_flag" rel="_email_notify" type="checkbox" name="track_no_email_notify" value="1" <?php echo ($track_no_email_notify ? ' checked="checked"' : ''); ?>/></label>
            </td>
          </tr>
		  
		  <tr class="_email_notify">
			<td>Текст письма:</td>
			<td>
				<textarea name="track_no_email_text" rows="10" cols="100" class="form-control"><?php echo $track_no_email_text; ?></textarea>
                <br><span class="help">Подстановка в письмо: {order_id} - номер заказа, {track_no} - трек-номер, {shipping_firstname} и {shipping_lastname} - имя и фамилия покупателя.</span>
			</td>
		  </tr>

		  <tr>
			<td>Уведомлять покупателя по SMS:</td>
            <td><label><input class="_form_flag" rel="_sms_notify" type="checkbox" name="track_no_sms_notify" value="1" <?php echo ($track_no_sms_notify ? ' checked="checked"' : ''); ?>/></label>
				<?php if (!$sms_on) { ?><span style="color: red;" data-toggle="tooltip" title="Перейдите в раздел Система - Настройки - Мой магазин, и там включите SMS-отправление">SMS-информирование у вас отключено, либо не поддерживается. SMS отправляться не будут.</span><?php } ?>
            </td>
		  </tr>
		  
		  <tr class="_sms_notify">
			<td>Текст SMS:</td>
			<td>
				<textarea name="track_no_sms_text" rows="5" cols="100" class="form-control"><?php echo $track_no_sms_text; ?></textarea>
                <br><span class="help">Подстановка в SMS: {order_id} - номер заказа, {track_no} - трек-номер, {shipping_firstname} и {shipping_lastname} - имя и фамилия покупателя.</span>
			</td>
		  </tr>

		  <tr>
			<td>Экспортировать в <a href="http://www.liveinform.ru/?partner=2324" target="_blank">LiveInform.ru</a>:
			<td>
                <label><input class="_form_flag" rel="_export_liveinform" type="checkbox" name="track_no_export_liveinform" value="1" <?php echo ($track_no_export_liveinform ? ' checked="checked"' : ''); ?>/></label>
                <br><span class="help">Работу с уведомлениями СДЭК необходимо включить в личном кабинете <a href="http://www.liveinform.ru/?partner=2324" target="_blank">LiveInform.ru</a></span>
			</td>
		  </tr>
		  
		  <tr class="_export_liveinform">
			<td>API ID:</td>
			<td>
				<input type="text" name="track_no_liveinform_api_id" value="<?php echo $track_no_liveinform_api_id; ?>" class="form-control input-small" />
				<a href="http://www.liveinform.ru/?partner=2324">получить API ID</a>
			</td>
		  </tr>
		  
		  <tr class="_export_liveinform">
            <td>Синхронизировать статусы <a href="http://www.liveinform.ru/?partner=2324" target="_blank">LiveInform</a>:</td>
			<td>
                <label><input class="_form_flag" rel="_liveinform_sync" type="checkbox" name="track_no_liveinform_sync" value="1" <?php echo ($track_no_liveinform_sync ? ' checked="checked"' : ''); ?>/></label>
			</td>
		  </tr>
              
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Скрипт синхронизации:</td>
			<td>
				<a href="<?php echo $liveinform_sync_url; ?>" target="_blank"><?php echo $liveinform_sync_url; ?></a>
			</td>
		  </tr>
              
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Не обрабатывать заказы с этими статусами:</td>
            <td>
              <select name="track_no_order_statuses[]" id="order-statuses" size="15" multiple class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"<?php echo (in_array($order_status['order_status_id'], $track_no_order_statuses) ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
				<?php } ?>
              </select>
            </td>
		  </tr>
		  
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Статус заказа, когда он "В пути":</td>
            <td>
              <select name="track_no_shipping_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $track_no_shipping_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </td>
		  </tr>
		  
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Статус заказа, когда он "На почте":</td>
            <td>
              <select name="track_no_postoffice_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $track_no_postoffice_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </td>
		  </tr>
		  
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Статус заказа, когда он "Вручен":</td>
            <td>
              <select name="track_no_issued_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $track_no_issued_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </td>
		  </tr>
		  
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Статус заказа, когда "Возврат":</td>
            <td>
              <select name="track_no_return_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $track_no_return_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </td>
		  </tr>
		  
		  <tr class="_export_liveinform _liveinform_sync">
            <td>Текст комментария:</td>
			<td>
				<textarea name="track_no_sync_comment" rows="5" cols="100" class="form-control"><?php echo $track_no_sync_comment; ?></textarea>
                <br><span class="help">Подстановка в комментарий: {order_id} - номер заказа, {track_no} - трек-номер, {shipping_firstname} и {shipping_lastname} - имя и фамилия покупателя.</span>
			</td>
		  </tr>
		  
		</table>
			
		<input type="hidden" name="track_no_set" value="1" />
      </form>
  </div>
</div>
<script>
$(document).ready(function() {

$('._form_flag').change(function() {
	if ($(this).is(':checked')) {
		$('.'+$(this).attr('rel')).show();
	}
	else {
		$('.'+$(this).attr('rel')).hide();
	}
});
$('._form_flag').trigger('change');


})
</script>
<?php echo $footer; ?>
