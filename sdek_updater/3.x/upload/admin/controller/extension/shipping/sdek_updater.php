<?php
/**
* SDEK shipping tracking for OpenCart (ocStore) 3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
* 
* Official version of this module: ...
*/

class ControllerExtensionShippingSdekUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 3;
	public $CONFIG = array(
		'status'=>true,
		'Account' => '',
		'Secure_password' => '',
		'Timezone_diff'=>-4,
		'shipping_code'=>'',
		'sendcity_postcode'=>'',
        
        'assign'=>false,
        
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);
	private $last_hours = 12;
	private $last_error = '';

	public $STATUS_MAPPING = array(
     /*
	 'a' => array('name'=>'Присвоен трек-номер',
		'notify'=>'true', 'enabled'=>true, 'status'=>17, 'text'=>'Заказ зарегистрирован в базе данных СДЭК, вы можете отслеживать заказ на сайте: http://www.edostavka.ru/nakladnoy/?RegistrNum={track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.edostavka.ru, код отслеживания: {track_no}'),
     */
    
	 1 => array('name'=>'Создан',
		'notify'=>'true', 'enabled'=>true, 'status'=>17, 'text'=>'Заказ зарегистрирован в базе данных СДЭК, вы можете отслеживать заказ на сайте: http://www.edostavka.ru/nakladnoy/?RegistrNum={track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.edostavka.ru, код отслеживания: {track_no}'),
	 2 => array('name'=>'Удален',
		'notify'=>'true', 'enabled'=>true, 'status'=>9, 'text'=>'Заказ отменен ИМ после регистрации в системе до прихода груза на склад СДЭК в городе-отправителе'),
	 3 => array('name'=>'Принят на склад отправителя',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Оформлен приход на склад СДЭК в городе-отправителе'),
	 6 => array('name'=>'Выдан на отправку в г.-отправителе',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Оформлен расход со склада СДЭК в городе-отправителе. Груз подготовлен к отправке (консолидирован с другими посылками)'),
	16 => array('name'=>'Возвращен на склад отправителя',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Повторно оформлен приход в городе-отправителе (не удалось передать перевозчику по какой-либо причине)'),
	 7 => array('name'=>'Сдан перевозчику в г.-отправителе',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в городе-отправителе. Консолидированный груз передан на доставку (в аэропорт/загружен машину)'),
	21 => array('name'=>'Отправлен в г.-транзит',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в город-транзит. Проставлены дата и время отправления у перевозчика'),
	22 => array('name'=>'Встречен в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована встреча в городе-транзите'),
	13 => array('name'=>'Принят на склад транзита',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен приход в городе-транзите'),
	17 => array('name'=>'Возвращен на склад транзита',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Повторно оформлен приход в городе-транзите (груз возвращен на склад)'),
	19 => array('name'=>'Выдан на отправку в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Повторно оформлен приход в городе-транзите (груз возвращен на склад)'),
	20 => array('name'=>'Сдан перевозчику в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка у перевозчика в городе-транзите'),
	 8 => array('name'=>'Отправлен в г.-получатель',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в город-получатель, груз в пути'),
	 9 => array('name'=>'Встречен в г.-получателе',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована встреча груза в городе-получателе'),
	10 => array('name'=>'Принят на склад доставки',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен приход на склад города-получателя., ожидает доставки до двери'),
	12 => array('name'=>'Принят на склад до востребования',
		'notify'=>'true', 'enabled'=>true, 'status'=>22, 'text'=>'Оформлен приход на склад города-получателя. Доставка до склада, посылка ожидает забора клиентом - покупателем ИМ',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его по адресу: {address}'),
	11 => array('name'=>'Выдан на доставку',
		'notify'=>'true', 'enabled'=>true, 'status'=>18, 'text'=>'Добавлен в курьерскую карту, выдан курьеру на доставку'),
	18 => array('name'=>'Возвращен на склад доставки',
		'notify'=>'true', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен повторный приход на склад в городе-получателе. Доставка не удалась по какой-либо причине, ожидается очередная попытка доставки'),
	4 => array('name'=>'Вручен',
		'notify'=>'false', 'enabled'=>true, 'status'=>5, 'text'=>'Успешно доставлен и вручен адресату'),
	5 => array('name'=>'Не вручен, возврат',
		'notify'=>'true', 'enabled'=>true, 'status'=>7, 'text'=>'Покупатель отказался от покупки, возврат в ИМ')
	);

	private function setConfig() {
		if (!$this->config->get('sdekupd_set')) {
			foreach($this->STATUS_MAPPING as $code=>$vals) {
				$this->STATUS_MAPPING[$code]['text'] = "{DATE} {CITY}\n".$vals['text'];
			}
			return;
		}
	
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('sdekupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('sdek_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('sdekupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('sdekupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('sdekupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('sdekupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('sdekupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('sdekupd_mapping'.$code.'_smsnotify');
			}
		}
    }
	
	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='sdekupd'");
	}
	
	public function index() {
		$this->load->language('extension/shipping/sdek_updater');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('sdekupd', $this->request->post);	
			$module_status = array('shipping_sdek_updater_status'=>$this->request->post['sdekupd_status']);
			$this->model_setting_setting->editSetting('shipping_sdek_updater', $module_status);	
            
			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'));
			return;
		}
        
		$data = array();
		$this->setConfig();
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['cron_run'] = 'php '.realpath(DIR_CATALOG.'../sdek_updater.php');
		$data['wget_run'] = 'wget -O - '.HTTPS_CATALOG.'index.php?route=api/sdek_updater/update';

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/shipping/sdek_updater', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('extension/shipping/sdek_updater', 'user_token=' . $this->session->data['user_token'], 'SSL');
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'); 		
		
		
		$this->load->model('localisation/order_status');
    	$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['sdekupd_'.$key])) {
				$data['sdekupd_'.$key] = $this->request->post['sdekupd_'.$key];
			} else {
				$data['sdekupd_'.$key] = $conf;
			}
		}
		
		$data['status_mapping'] = $this->STATUS_MAPPING;

		if (isset($this->request->post['sdek_updater_status'])) {
			$data['sdek_updater_status'] = $this->request->post['sdekupd_status'];
		} else {
			$data['sdek_updater_status'] = $this->CONFIG['status'];
		}
		
		foreach($this->STATUS_MAPPING as $code=>$val) {
			if (isset($this->request->post['sdekupd_mapping'.$code.'_notify'])) {
				$data['sdekupd_mapping'.$code.'_notify'] = $this->request->post['sdekupd_mapping'.$code.'_notify'];
			} else {
				$data['sdekupd_mapping'.$code.'_notify'] = $val['notify'];
			}
			if (isset($this->request->post['sdekupd_mapping'.$code.'_smsnotify'])) {
				$data['sdekupd_mapping'.$code.'_smsnotify'] = $this->request->post['sdekupd_mapping'.$code.'_smsnotify'];
			} elseif (isset($val['smsnotify'])) {
				$data['sdekupd_mapping'.$code.'_smsnotify'] = $val['smsnotify'];
			}
			
			if (isset($this->request->post['sdekupd_mapping'.$code.'_enabled'])) {
				$data['sdekupd_mapping'.$code.'_enabled'] = $this->request->post['sdekupd_mapping'.$code.'_enabled'];
			} else {
				$data['sdekupd_mapping'.$code.'_enabled'] = $val['enabled'];
			}
			
			if (isset($this->request->post['sdekupd_mapping'.$code.'_status'])) {
				$data['sdekupd_mapping'.$code.'_status'] = $this->request->post['sdekupd_mapping'.$code.'_status'];
			} else {
				$data['sdekupd_mapping'.$code.'_status'] = $val['status'];
			}
			
			if (isset($this->request->post['sdekupd_mapping'.$code.'_text'])) {
				$data['sdekupd_mapping'.$code.'_text'] = $this->request->post['sdekupd_mapping'.$code.'_text'];
			} else {
				$data['sdekupd_mapping'.$code.'_text'] = $val['text'];
			}

			if (isset($this->request->post['sdekupd_mapping'.$code.'_sms'])) {
				$data['sdekupd_mapping'.$code.'_sms'] = $this->request->post['sdekupd_mapping'.$code.'_sms'];
			} elseif (isset($val['sms'])) {
				$data['sdekupd_mapping'.$code.'_sms'] = $val['sms'];
			}
		}
		
		$data['sms_gatenames'] = array();
		$files = glob(DIR_SYSTEM . 'smsgate/*.php');
		foreach ($files as $file) {
			$data['sms_gatenames'][] =  basename($file, '.php');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/shipping/sdek_updater', $data));		
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/sdek_updater')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
