<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<meta http-equiv="Content-Type" content="text/html; charset="UTF-8" />
<title>Доставка СДЭК</title>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="data_form">
<form id="form1" name="form1" action="<?php echo $form_action; ?>" method="post">
<table width="850" border="0" cellspacing="3" cellpadding="1" align="center">
	<?php if (isset($sdek_error)) { ?>
	<tr>
		<td colspan="2"><div class="warning"><?php echo $sdek_error; ?></div></td>
	</tr>
	<?php } ?>
	<?php if (isset($order_info['track_no']) && $order_info['track_no'] != '') { ?>
	<tr>
		<td colspan="2"><div class="success">Заказ зарегистрирован в службе доставки. Выдан трек-номер: <b><?php echo $order_info['track_no']; ?></b></div></td>
	</tr>
	<?php } ?>
   <tr>
	<td width="50%" valign="top">
	<table width="425" border="0" cellspacing="3" cellpadding="1">
		<tr>
			<td colspan="2"><h3>Отправление</h3></td>
		</tr>
		<tr>
			<td width="132"><label for="arr[TariffTypeCode]">Код типа тарифа:</label></td>
			<td width="300">
				<select name="arr[TariffTypeCode]" id="TariffTypeCode">
					<option value="137"<?php echo ($arr['TariffTypeCode']==137 ? ' selected="selected"' : '');?>>Посылка склад-дверь</option>
					<!--<option value="136"<?php echo ($arr['TariffTypeCode']==136 ? ' selected="selected"' : '');?>>Посылка склад-склад</option>-->
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Дополнительные услуги:</label></td>
			<td>
			<?php foreach ($ServiceCodes as $code=>$name) { ?>
				<div><input name="arr[ServiceCode][]" id="ServiceCode_<?php echo $code; ?>" type="checkbox" value="<?php echo $arr['AddService']; ?>"<?php echo (in_array($code, $arr['ServiceCode']) ? ' checked="checked"' : ''); ?>/>
				<label for="ServiceCode_<?php echo $code; ?>"><?php echo $name; ?></label></div>
			<?php } ?>
			</td>
		</tr>
		<tr>
			<td><label for="arr[BarCode]">Штрих-код упаковки:</label></td>
			<td><input name="arr[BarCode]" type="text" id="BarCode" value="<?php echo $arr['BarCode'];?>" size="30" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[Weight]">Вес в граммах:</label></td>
			<td><input type="text" name="arr[Weight]" id="Weight" value="<?php echo $arr['Weight'];?>" size="10" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[Comment]">Комментрий к заказу:</label></td>
			<td><textarea name="arr[Comment]" id="Comment" rows="4" cols="30"><?php echo $arr['Comment'];?></textarea></td>
		</tr>
		

    	</table>
	</td>
	
	<td width="50%" valign="top">
	<table width="425" border="0" cellspacing="3" cellpadding="1">
		<tr>
			<td colspan="2"><h3>Получатель</h3></td>
		</tr>
		<tr>
			<td width="132"><label for="arr[RecipientName]">Ф.И.О.:</label></td>
			<td width="300"><input name="arr[RecipientName]" type="text" id="RecipientName" value="<?php echo $arr['RecipientName'];?>" size="30" required="required"/></td>
		</tr>
		<!--
		<tr>
			<td><label for="arr[RecCityCode]">Город:</label></td>
			<td><input type="text" name="arr[RecCityCode]" id="RecCityCode" value="<?php echo $arr['RecCityCode'];?>" size="30" required="required"/>
			<br/>Указан в заказе: <b><?php echo $order_info['shipping_city']; ?></b>
			</td>
		</tr>
		-->
		<tr>
			<td><label for="arr[RecCityPostCode]">Почтовый индекс:</label></td>
			<td><input type="text" name="arr[RecCityPostCode]" id="RecCityPostCode" value="<?php echo $arr['RecCityPostCode'];?>" size="10" maxlength="6" required="required"/>
			<br/>Город: <b><?php echo $order_info['shipping_zone'].', '.$order_info['shipping_city']; ?></b>
			</td>
		</tr>
		<tr>
			<td><label for="arr[Street]">Улица:</label></td>
			<td><input type="text" name="arr[Street]" id="Street" value="<?php echo $arr['Street'];?>" size="45" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[House]">Дом, корпус, строение:</label></td>
			<td><input type="text" name="arr[House]" id="House" value="<?php echo $arr['House'];?>" size="10" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[Flat]">Квартира/Офис:</label></td>
			<td><input type="text" name="arr[Flat]" id="Flat" value="<?php echo $arr['Flat'];?>" size="10" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[Phone]">Телефон:</label></td>
			<td><input type="text" name="arr[Phone]" id="Phone" value="<?php echo $arr['Phone'];?>" size="30" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[RecipientEmail]">E-Mail:</label></td>
			<td><input type="text" name="arr[RecipientEmail]" id="RecipientEmail" value="<?php echo $arr['RecipientEmail'];?>" size="30" required="required"/></td>
		</tr>
		<tr>
			<td><label for="arr[TimeBeg]">Дата и время вручения:</label></td>
			<td>
				<input placeholder="гггг-мм-дд" type="text" name="arr[AttemptDate]" id="AttemptDate" value="<?php echo $arr['AttemptDate'];?>" size="10" maxlength="10" required="required"/>
				<input placeholder="чч:мм" type="text" name="arr[TimeBeg]" id="TimeBeg" value="<?php echo $arr['TimeBeg'];?>" size="6" maxlength="5" required="required"/>-
				<input placeholder="чч:мм" type="text" name="arr[TimeEnd]" id="TimeEnd" value="<?php echo $arr['TimeEnd'];?>" size="6" maxlength="5" required="required"/>
			</td>
		</tr>
  	</table>
	</td>	
  </tr>

  <tr>
    <td colspan="2" align="center">
		<input type="submit" name="send" value="Отправить" id="but_send"<?php echo ((isset($order_info['track_no']) && $order_info['track_no'] != '') ? ' disabled="disabled"' : ''); ?>/>
	</td>
  </tr>
</table>
</form>
</div>
<script type="text/javascript">
$(document).ready(function() {

})
</script>
</body>
</html>