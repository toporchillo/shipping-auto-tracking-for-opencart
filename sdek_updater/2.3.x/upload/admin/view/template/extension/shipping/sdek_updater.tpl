<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> Редактировать настройки модуля</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status">Статус:</label>
            <div class="col-sm-10">
              <select name="sdekupd_status" id="input-status" class="form-control">			
                <?php if ($sdekupd_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>	

		  <div class="form-group">
			<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Запускать через wget">Скрипт обновления:</span></label>
			<div class="col-sm-10">
				<a href="<?php echo HTTPS_CATALOG; ?>index.php?route=api/sdek_updater/update"><?php echo HTTPS_CATALOG; ?>index.php?route=api/sdek_updater/update</a> (wget) 
				<!--
                <br>
				<?php echo DIR_CATALOG; ?>sdek_updater.php (PHP-CLI)
				-->
			</div>
		  </div>
		  
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="sdekupd_Account"><span>СДЭК Account:</span></label>
			<div class="col-sm-10">
				<input type="text" name="sdekupd_Account" value="<?php echo $sdekupd_Account; ?>" class="form-control input-small" required />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="sdekupd_Secure_password"><span>СДЭК Secure password:</span></label>
			<div class="col-sm-10">
				<input type="text" name="sdekupd_Secure_password" value="<?php echo $sdekupd_Secure_password; ?>" class="form-control input-small" required />
			</div>
		  </div>
          
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="sdekupd_Timezone_diff"><span>Разница времени СДЭК и вашего сервера, ч:</span></label>
			<div class="col-sm-10">
				<input type="text" name="sdekupd_Timezone_diff" value="<?php echo $sdekupd_Timezone_diff; ?>" class="form-control input-small" style="width:60px;" required />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="sdekupd_sendcity_postcode"><span>Почтовый индекс отправителя:</span></label>
			<div class="col-sm-10">
				<input type="text" name="sdekupd_sendcity_postcode" value="<?php echo $sdekupd_sendcity_postcode; ?>" class="form-control input-small" style="width:120px;" required />
			</div>
		  </div>
          
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="sdekupd_assign_cb"><span data-toggle="tooltip" title="Если вы указали номер заказа при отправлении">Присваивать трек-номера автоматически:</span></label>
			<div class="col-sm-10">
				<div class="checkbox"><label><input type="checkbox" id="sdekupd_assign_cb" name="sdekupd_assign" value="1" <?php echo ($sdekupd_assign ? ' checked="checked"' : ''); ?>/></label></div>
			</div>
		  </div>
          
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-smsgate">SMS шлюз:</label>
          <div class="col-sm-10">
            <select name="sdekupd_sms_gatename" id="select-smsgate" class="form-control">
              <?php foreach ($sms_gatenames as $sms_gatename) { ?>
              <?php if ($sdekupd_sms_gatename == $sms_gatename) { ?>
              <option value="<?php echo $sms_gatename; ?>" selected="selected"><?php echo $sms_gatename; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sms_gatename; ?>"><?php echo $sms_gatename; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <br />
            <span id="sms_gate_link"></span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-sms-gate-username">Логин или ID на SMS шлюзе:</label>
          <div class="col-sm-10">
            <input type="text" name="sdekupd_sms_gate_username" value="<?php echo $sdekupd_sms_gate_username; ?>" id="input-sms-gate-username" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-config-sms-gate-password">Пароль или token на SMS шлюзе:</label>
          <div class="col-sm-10">
            <input type="password" name="sdekupd_sms_gate_password" value="<?php echo $sdekupd_sms_gate_password; ?>" id="input-sms-gate-password" class="form-control" />
          </div>
        </div>	
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-config-sms-gate-from">Отправитель SMS:</label>
          <div class="col-sm-10">
            <input type="text" name="sdekupd_sms_gate_from" value="<?php echo $sdekupd_sms_gate_from; ?>" id="input-sms-gate-from" class="form-control" />
          </div>
        </div>          
          
          <table class="form">
            <tr>
              <td colspan="2">Подстановка данных в уведомления:</td>
              <td colspan="3">
              <span class="help">{order_id} - номер заказа, {track_no} - код отслеживания, {firstname},{lastname} - имя и фамилия покупателя, {address} - адрес доставки.</span>
              <br/><span class="help">{DATE} - дата смены статуса, {CITY} - город, где находится доставка.</span>
              </td>
            </tr>
              
            <tr align="left">
            <th width="20%">Статус заказа в СДЭК</th>
            <th width="5%">Обраб.</th>
            <th width="10%">Статус заказа в OpenCart</th>
            <th width="60%">Писать в историю заказа / Текст SMS-уведомления</th>
            <th width="10%">Уведомлять покупателя</th>
            </tr>
            
			<?php foreach ($status_mapping as $code=>$vals) { ?>
            
            <tr>
              <td><?php echo $code.': '.$vals['name'] ?></td>
              
              <td>
              <input type="checkbox" name="<?php echo 'sdekupd_mapping'.$code.'_enabled';?>" value="1"<?php echo ($vals['enabled'] ? ' checked' : ''); ?>/>
              </td>
              
              <td><select name="<?php echo 'sdekupd_mapping'.$code.'_status';?>">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $vals['status']) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
              </td>

              <td align="center">
              <textarea name="<?php echo 'sdekupd_mapping'.$code.'_text';?>" rows="3" style="width: 90%"><?php echo $vals['text']; ?></textarea>
              <textarea name="<?php echo 'sdekupd_mapping'.$code.'_sms';?>" rows="2"  style="width: 90%" placeholder="SMS-уведомление"><?php echo (isset($vals['sms']) ? $vals['sms'] : ''); ?></textarea>
              </td>
              
              <td>
              <label><input type="checkbox" name="<?php echo 'sdekupd_mapping'.$code.'_notify';?>" value="1"<?php echo ($vals['notify'] ? ' checked' : ''); ?>/>Email</label>
              <label><input type="checkbox" name="<?php echo 'sdekupd_mapping'.$code.'_smsnotify';?>" value="1"<?php echo (isset($vals['smsnotify']) && $vals['smsnotify'] ? ' checked' : ''); ?>/>SMS</label>
              </td>
              
            </tr>
            <tr><td colspan="5"><hr></td></tr>
              
              <?php } ?>
          </table>          
          
		  
				
		<input type="hidden" name="sdekupd_set" value="1" />
      </form>
    </div>
  </div>
</div>
<script>
var smsGates = {
    bytehand: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
    bytehandcom: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
	smsc: 'https://smsc.ru/?ppOpenCart',
    smscru: 'https://smsc.ru/?ppOpenCart',
    smscua: 'https://smsc.ru/?ppOpenCart',
	epochtasms: 'http://www.epochta.ru/#a_aid=opencart',
	epochta: 'http://www.epochta.ru/#a_aid=opencart',
    epochtasmscomua: 'http://www.epochta.ru/#a_aid=opencart',
	unisender: 'http://www.unisender.com/?a=opencart',
    unisenderru: 'http://www.unisender.com/?a=opencart'
};
$('#select-smsgate').change(function() {
	var gate = $(this).val();
	if (smsGates[gate]) {
		$('#sms_gate_link').html('<a href="'+smsGates[gate]+'" target="_blank">Получить доступ</a>');
	}
	else {
		$('#sms_gate_link').html('');
	}
});
$('#select-smsgate').trigger('change');

</script>
<?php echo $footer; ?>
