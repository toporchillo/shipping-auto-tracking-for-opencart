<?php
// Heading
$_['heading_title']      = 'Автотреккинг доставок СДЭК';
$_['text_success']       = 'Настройки модуля обновлены!';
$_['text_shipping']      = 'Доставка';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим модулем!';
?>
