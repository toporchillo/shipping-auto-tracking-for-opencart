<?php
/**
* SDEK shipping tracking for OpenCart (ocStore) 2.0.x - 2.2.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: ...
*/

class ControllerApiSdekUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'status'=>true,
		'Account' => '',
		'Secure_password' => '',
		'Timezone_diff'=>-4,
		'shipping_code'=>'',
		'sendcity_postcode'=>'',
        
        'assign'=>false,
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);

	private $last_hours = 12;
	private $last_error = '';

	public $STATUS_MAPPING = array(
    /*
	 'a'=> array('name'=>'Присвоен трек-номер',
		'notify'=>'true', 'enabled'=>true, 'status'=>17, 'text'=>'Заказ зарегистрирован в базе данных СДЭК, вы можете отслеживать заказ на сайте: http://www.edostavka.ru/nakladnoy/?RegistrNum={track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.edostavka.ru, код отслеживания: {track_no}'),
    */
        
	 1 => array('name'=>'Создан',
		'notify'=>'true', 'enabled'=>true, 'status'=>17, 'text'=>'Заказ зарегистрирован в базе данных СДЭК, вы можете отслеживать заказ на сайте: http://www.edostavka.ru/nakladnoy/?RegistrNum={track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.edostavka.ru, код отслеживания: {track_no}'),
	 2 => array('name'=>'Удален',
		'notify'=>'true', 'enabled'=>true, 'status'=>9, 'text'=>'Заказ отменен ИМ после регистрации в системе до прихода груза на склад СДЭК в городе-отправителе'),
	 3 => array('name'=>'Принят на склад отправителя',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Оформлен приход на склад СДЭК в городе-отправителе'),
	 6 => array('name'=>'Выдан на отправку в г.-отправителе',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Оформлен расход со склада СДЭК в городе-отправителе. Груз подготовлен к отправке (консолидирован с другими посылками)'),
	16 => array('name'=>'Возвращен на склад отправителя',
		'notify'=>'false', 'enabled'=>true, 'status'=>17, 'text'=>'Повторно оформлен приход в городе-отправителе (не удалось передать перевозчику по какой-либо причине)'),
	 7 => array('name'=>'Сдан перевозчику в г.-отправителе',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в городе-отправителе. Консолидированный груз передан на доставку (в аэропорт/загружен машину)'),
	21 => array('name'=>'Отправлен в г.-транзит',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в город-транзит. Проставлены дата и время отправления у перевозчика'),
	22 => array('name'=>'Встречен в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована встреча в городе-транзите'),
	13 => array('name'=>'Принят на склад транзита',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен приход в городе-транзите'),
	17 => array('name'=>'Возвращен на склад транзита',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Повторно оформлен приход в городе-транзите (груз возвращен на склад)'),
	19 => array('name'=>'Выдан на отправку в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Повторно оформлен приход в городе-транзите (груз возвращен на склад)'),
	20 => array('name'=>'Сдан перевозчику в г.-транзите',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка у перевозчика в городе-транзите'),
	 8 => array('name'=>'Отправлен в г.-получатель',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована отправка в город-получатель, груз в пути'),
	 9 => array('name'=>'Встречен в г.-получателе',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Зарегистрирована встреча груза в городе-получателе'),
	10 => array('name'=>'Принят на склад доставки',
		'notify'=>'false', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен приход на склад города-получателя., ожидает доставки до двери'),
	12 => array('name'=>'Принят на склад до востребования',
		'notify'=>'true', 'enabled'=>true, 'status'=>22, 'text'=>'Оформлен приход на склад города-получателя. Доставка до склада, посылка ожидает забора клиентом - покупателем ИМ',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его по адресу: {address}'),
	11 => array('name'=>'Выдан на доставку',
		'notify'=>'true', 'enabled'=>true, 'status'=>18, 'text'=>'Добавлен в курьерскую карту, выдан курьеру на доставку'),
	18 => array('name'=>'Возвращен на склад доставки',
		'notify'=>'true', 'enabled'=>true, 'status'=>18, 'text'=>'Оформлен повторный приход на склад в городе-получателе. Доставка не удалась по какой-либо причине, ожидается очередная попытка доставки'),
	4 => array('name'=>'Вручен',
		'notify'=>'false', 'enabled'=>true, 'status'=>5, 'text'=>'Успешно доставлен и вручен адресату'),
	5 => array('name'=>'Не вручен, возврат',
		'notify'=>'true', 'enabled'=>true, 'status'=>7, 'text'=>'Покупатель отказался от покупки, возврат в ИМ')
	);
    
	private function setConfig() {
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('sdekupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('sdek_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('sdekupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('sdekupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('sdekupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('sdekupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('sdekupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('sdekupd_mapping'.$code.'_smsnotify');
			}
		}
	}

	/**
	Присвоить заказу трек-номер
	**/
	public function assign($order_id, $track_no) {
		if (!$this->CONFIG['assign']) {
			return false;
		}
		$this->db->query("UPDATE `".DB_PREFIX."order` SET track_no='".$this->db->escape($track_no)."' WHERE order_id='".(int)$order_id."' AND track_no=''");
		$this->log("Найден трек-номер #".$track_no." для заказа ID:".$order_id, 4);
		if ($this->db->countAffected()) {
			$this->log("Заказ ID:".$order_id." присвоен трек-номер: ".$track_no, 3);
		}
	}
	
	public function update() {
		$this->setConfig();
    	$this->language->load('sale/order');
	
		if (isset($_SERVER['HTTP_HOST'])) {
			header("Content-Type: text/html; charset=utf-8");
		}
		$this->log('SDEK-tracking Started', 3);
		if (!$this->config->get('sdek_updater_status')) {
			$this->log('Модуль "Автотреккинг доставок СДЭК" отключен', 3);
			return false;
		}
		
        
		$from_date = date('Y-m-d\TH:i:s', time() + $this->CONFIG['Timezone_diff']*3600 - $this->last_hours*3600);
		$tuData = $this->getStatuses($from_date);
		if ($tuData !== false) {
			$res = $this->processStatusResponse($tuData);
			if ($res != false && is_array($res)) {
				foreach ($res as $track_no=>$state) {
					$this->handleStatus($track_no, $state);
				}
			}
		}
        
		$this->log('SDEK-tracking Finished', 3);
	}
	
	private function parseDate($str) {
		$date = date_parse($str);
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
    
    /*
	private function addReward($order) {
        if (!$order['customer_id']) {
            return 0; //Некому назначать
        }
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order['order_id'] . "' AND points > 0");
        if ($query->row['total']) {
            return 0;  //Уже назначено
        }
        $reward = 0;
        $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order['order_id'] . "'");
        foreach ($order_product_query->rows as $product) {
            $reward += $product['reward'];
        }
        if (!$reward) {
            return 0; //0 баллов - не надо назначать
        }
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$order['customer_id'] . "', order_id = '" . (int)$order['order_id'] . "', points = '" . (int)$reward . "', description = 'Заказ #" . (int)$order['order_id'] . "', date_added = NOW()");
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Начисляем бонусы: '.$reward.' для покупателя ID:'.$order['customer_id'], 4);
        return $order['points'];
	}
    */
    
    //============
    
	private function getStatuses($from_date) {
		$date = date('Y-m-d\TH:i:s', time() + $this->CONFIG['Timezone_diff']*3600);
		$secure = md5($date.'&'.$this->CONFIG['Secure_password']);
		$request = '<?xml version="1.0" encoding="UTF-8" ?>
				<StatusReport Date="'.$date.'" Account="'.$this->CONFIG['Account'].'" Secure="'.$secure.'" ShowHistory="1">
				<ChangePeriod DateFirst="'.$from_date.'" DateLast="'.$date.'"/>
			</StatusReport>';
			
		return $this->postRequest('https://integration.cdek.ru/status_report_h.php', $request);
	}
	
	/**
	* Отправка POST-запроса серверу СДЭК
	**/
	private function postRequest($url, $request) {
		$this->log('Sending request to '.$url.":\n" . $request, 5);
		
		$tuCurl = curl_init();		
		curl_setopt($tuCurl, CURLOPT_URL, $url);
		curl_setopt($tuCurl, CURLOPT_HEADER, 0);
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($tuCurl, CURLOPT_POST, 1);
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, array('xml_request'=>$request));

		$tuData = curl_exec($tuCurl);
		if(curl_errno($tuCurl)){
			$info = curl_getinfo($tuCurl);
			$this->log('Curl Error: '.curl_error($tuCurl).'. Took: ' . $info['total_time'] . 'sec. URL: ' . $info['url'], 1);
			return false;
		} else {
			return $tuData;
		}
	}
	
	/**
	* Обработка ответа со статусами заказов от СДЭК
	**/
	private function processStatusResponse($tuData) {
		try {
			$xml = simplexml_load_string($tuData);
			$attr = $xml->Attributes();
			if (isset($attr['ErrorCode'])) {
				$this->log('SDEK returns error: "'.$attr['ErrorCode'].": ".$attr['Msg'], 1);
			}
			elseif (!isset($xml->Order) || !count($xml->Order)) {
				$this->log('SDEK returns empty order history.', 4);
			}
			else {
				$ret = array();
				foreach($xml->Order as $order) {
					$oattr = $order->Attributes();
					$ret[strval($oattr['DispatchNumber'])] = array();
                    if (isset($oattr['Number']) && intval($oattr['Number']) && $oattr['DispatchNumber']) {
                        $this->assign($oattr['Number'], $oattr['DispatchNumber']);
                    }

					$sattr = $order->Status->Attributes();
					$ret[strval($oattr['DispatchNumber'])] = array(
						'Date' => strval($sattr['Date']),
						'Code' => strval($sattr['Code']),
						'Description' => strval($sattr['Description']),
						'CityName' => strval($sattr['CityName']),					
					);
					/*
					foreach($order->Status->State as $state) {
						$sattr = $state->Attributes();
						$ret[strval($oattr['DispatchNumber'])][] = array(
							'Date' => strval($sattr['Date']),
							'Code' => strval($sattr['Code']),
							'Description' => strval($sattr['Description']),
							'CityName' => strval($sattr['CityName']),					
						);
					}
					*/
				}
				$this->log('SDEK returns order states: '.json_encode($ret), 4);
				return($ret);
			}
		} catch (Exception $e) {
			$this->log("SDEK returns invalid XML (".$e->getMessage()."): \n".$tuData, 1);
		}
		return false;	
	}

	private function handleStatus($track_no, $state) {
		if (!isset($this->STATUS_MAPPING[$state['Code']]) || !$this->STATUS_MAPPING[$state['Code']]['enabled']) {
			$this->log('Order #'.$track_no.', status: '.$state['Code'].'. Do nothing.', 3);
			return false;
		}

		$order_id = $this->findOrder($track_no);
		if (!$order_id) {
			$this->log('Order #'.$track_no.', status: '.$state['Code'].'. Order not found.', 1);
			return false;
		}
		$status = $this->STATUS_MAPPING[$state['Code']]['status'];
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);

		$comment = $this->getComment($state, $order_info);
		$q1 = $this->db->query("SELECT * FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order_id."' ORDER BY date_added DESC LIMIT 1");
		if ($q1->row && $q1->row['order_status_id'] == $status && strpos($q1->row['comment'], $comment) !== false) {
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['Code'].' ('.$status.'). Status already changed.', 3);
			return false;
		}
		
		try {
			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->STATUS_MAPPING[$state['Code']]['status'], $comment, $this->STATUS_MAPPING[$state['Code']]['notify'], true);
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['Code'].' ('.$status.'). Added order history.', 3);
			if (isset($this->STATUS_MAPPING[$state['Code']]['smsnotify']) && $this->STATUS_MAPPING[$state['Code']]['smsnotify']) {
				$this->smsNotify($order_info, $this->getComment($state, $order_info, 'sms'));
			}
			return true;
		} catch (Exception $e) {
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['Code'].' ('.$status.'). Unable to add order history ('.$e->getMessage().').', 1);
			return false;
		}	
	}

	/**
	* Получить ID заказа по номеру треккинга
	*/
	private function findOrder($track_no) {
		/*
		if ($this->CONFIG['shipping_code'] == '') {
			$sql = "SELECT order_id FROM `".DB_PREFIX."order_history` WHERE comment LIKE '%".$this->db->escape($track_no)."%' LIMIT 1";
		}
		else {
			$sql = "SELECT o.order_id FROM `".DB_PREFIX."order_history` h LEFT JOIN `".DB_PREFIX."order` o ON o.order_id=h.order_id
				WHERE o.shipping_code='".$this->db->escape($this->CONFIG['shipping_code'])."' AND h.comment LIKE '%".$this->db->escape($track_no)."%' LIMIT 1";
		}
		*/
		if ($this->CONFIG['shipping_code'] == '') {
			$sql = "SELECT order_id FROM `".DB_PREFIX."order` WHERE track_no='".$this->db->escape(trim($track_no))."' LIMIT 1";
		}
		else {
			$sql = "SELECT order_id FROM `".DB_PREFIX."order` WHERE track_no='".$this->db->escape(trim($track_no))."' 
				AND shipping_code='".$this->db->escape($this->CONFIG['shipping_code'])."' LIMIT 1";
		}
		
		$q = $this->db->query($sql);
		if (!$q->row) {
			return false;
		}
		else {
			return $q->row['order_id'];
		}
	}

	private function getComment($state, $order_info, $key='text') {
		$date = date('d.m.Y H:i:s', strtotime($state['Date']));
		$text = str_replace('{DATE}', $date, $this->STATUS_MAPPING[$state['Code']][$key]);
		$text = str_replace('{CITY}', $state['CityName'], $text);
		$text = str_replace('{firstname}', $order_info['firstname'], $text);
		$text = str_replace('{lastname}', $order_info['lastname'], $text);
		$text = str_replace('{order_id}', $order_info['order_id'], $text);
		$text = str_replace('{track_no}', $order_info['track_no'], $text);
		$address_arr = array($order_info['shipping_zone'], $order_info['shipping_city'], $order_info['shipping_address_1'], $order_info['shipping_address_2']);
		$address_arr = array_diff($address_arr, array(''));
		$text = str_replace('{address}', implode(', ', $address_arr), $text);
		return $text;
	}

	protected function smsNotify($order, $message) {
		$options = array(
			'to'       => $order['telephone'],
			'copy'     => '',
			'from'     => $this->CONFIG['sms_gate_from'],
			'username'    => $this->CONFIG['sms_gate_username'],
			'password' => $this->CONFIG['sms_gate_password'],
			'message'  => $message,
			'ext'      => null
		);
		
		if (!class_exists('Sms')) {
			require_once(DIR_SYSTEM . 'library/sms.php');
		}

		$sms = new Sms($this->config->get('sdekupd_sms_gatename'), $options);
		$sms->send();
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 4);
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'sdek_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}
