<?php
/**
* Boxberry shipping tracking for OpenCart (ocStore) 2.3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: ...
*/
class ControllerApiBoxberryUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'status'=>true,
		'token' => '',
        'mode'=>'test',
		'shipping_code'=>'',
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);
	private $last_error = '';

	public $STATUS_MAPPING = array(
	 1 => array('name'=>'Принято к доставке',
		'notify'=>true, 'enabled'=>true, 'status'=>2, 'text'=>'{firstname}, ваш заказ #{order_id} зарегистрирован в базе данных Boxberry, вы можете отслеживать заказ на сайте: http://www.boxberry.ru/ru/tracking/',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.boxberry.ru, код отслеживания: {track_no}'),
	 2 => array('name'=>'Передано на сортировку',
		'notify'=>false, 'enabled'=>true, 'status'=>2, 'text'=>'Заказ передан на пункт сортировки Boxberry.'),
	 3 => array('name'=>'Отправлено в город назначения',
		'notify'=>false, 'enabled'=>true, 'status'=>2, 'text'=>'Заказ отправлен в город назначения.'),
	 4 => array('name'=>'Поступило в пункт выдачи',
		'notify'=>true, 'enabled'=>true, 'status'=>3, 'text'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его в пункте выдачи Boxberry. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его в пункте выдачи Boxberry.  {COMMENT}'),
	 5 => array('name'=>'Передано на курьерскую доставку',
		'notify'=>true, 'enabled'=>true, 'status'=>2, 'text'=>'{firstname}, ваш заказ #{order_id} передан курьеру на доставку. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} передан курьеру на доставку. {COMMENT}'),
	 6 => array('name'=>'Выдано',
		'notify'=>false, 'enabled'=>true, 'status'=>3, 'text'=>'Заказ доставлен и вручен адресату. {COMMENT}'),
	 7 => array('name'=>'Готовится к возврату',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'{firstname}, вы не забрали заказ в пункте выдачи Boxberry. Заказ готовится к возврату. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, вы не забрали заказ #{order_id} в пункте выдачи Boxberry. Заказ готовится к возврату. {COMMENT}'),
	 8 => array('name'=>'Возвращено с курьерской доставки',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'{firstname}, курьер не смог вручить вам заказ #{order_id}. Заказ готовится к возврату. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, курьер не смог вручить вам заказ #{order_id}. Заказ готовится к возврату. {COMMENT}'),
	 9 => array('name'=>'Отправлено в пункт приема',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'Покупатель отказался от покупки, заказ отправлен в ИМ. {COMMENT}'),
	10 => array('name'=>'Возвращено в пункт приема',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'Недоставленный заказ вернулся в пункт приема. {COMMENT}'),
	11 => array('name'=>'Возвращено в ИМ',
		'notify'=>true, 'enabled'=>true, 'status'=>13, 'text'=>'Заказ возвращен в ИМ. {COMMENT}')
	);
	
	private function setConfig() {
		if (!$this->config->get('boxberryupd_set')) {
			foreach($this->STATUS_MAPPING as $code=>$vals) {
				$this->STATUS_MAPPING[$code]['text'] = "{DATE}\n".$vals['text'];
			}
			return;
		}
	
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('boxberryupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('boxberry_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('boxberryupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('boxberryupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('boxberryupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('boxberryupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('boxberryupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('boxberryupd_mapping'.$code.'_smsnotify');
			}
		}
	}
	
	public function update() {
		if ($this->ECHO) {
			header('Content-Type: text/html; charset=utf-8');
		}
		$this->setConfig();
		$this->load->model('checkout/order');
		if (!$this->config->get('boxberry_updater_status')) return false;

		$orders = $this->getOrdersToUpdate();
		foreach ($orders as $order) {
			try {
				//if (strlen(trim($order['track_no'])) < 8 || strlen(trim($order['track_no'])) >= 10) continue;
				$statuses = $this->getStatuses($order['track_no']);
				$this->handleStatus($order, $statuses);
			} catch(BoxberryException $e) {
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].') Boxberry-tracking error: ' . $e->getMessage(), 0);
			}
		}
	}
	
	protected function getOrdersToUpdate() {
		$shipping_code = $this->config->get('boxberryupd_shipping_code');
		$shcode_where = ($shipping_code ? " AND o.shipping_code LIKE '" . $shipping_code . "%'" : '');
		$not_in = ($this->config->get('boxberryupd_order_statuses') ? $this->config->get('boxberryupd_order_statuses') : '0');
		$query = $this->db->query("SELECT o.* FROM `" . DB_PREFIX . "order` o
			WHERE o.track_no <> '' AND o.track_no != '' $shcode_where AND o.order_status_id <> '0' AND NOT(o.order_status_id IN($not_in)) ORDER BY order_id DESC LIMIT 40");
		return $query->rows;
	}
	
	
	private function getStatuses($track_no) {
		$mode = $this->config->get('boxberryupd_mode');
		if ($mode == 'test') {
			$url='http://test.api.boxberry.de/json.php?token='.$this->config->get('boxberryupd_token').'&method=ListStatuses&ImId='.$track_no;
		}
		elseif ($mode == 'new') {
			$url='http://api.boxberry.ru/json.php?token='.$this->config->get('boxberryupd_token').'&method=ListStatuses&ImId='.$track_no;
		}
		else {
			$url='http://api.boxberry.de/json.php?token='.$this->config->get('boxberryupd_token').'&method=ListStatuses&ImId='.$track_no;
		}
		
		$handle = fopen($url, "rb");
		$contents = stream_get_contents($handle);
		fclose($handle);
		try {
			$data = json_decode($contents, true);
		}
		catch (Exception $e) {
			throw new BoxberryException('Boxberry API returns non-JSON response: '.$contents);
			return false;
		}
		if(count($data)<=0 || isset($data[0]['err'])) {
			 // если произошла ошибка, и ответ не был получен
			throw new BoxberryException($data[0]['err']);
			return false;
		}
		else {
			// все отлично, ответ получен, теперь в массиве $data
			// все статусы по данному заказу в формате:
			/*
				$data[0...n]=array(
					 'Date'=>'Дата статуса в формате ДД/ММ/ГГ Ч:М:C PM/AM',
					 'Name'=>'Наименование',
					 'Comment'=>'Комментарий'
				);
			*/
			return $data;
		}
	}
	
	private function handleStatus($order, $state) {
		$this->load->model('checkout/order');
		$return = false;
		foreach ($state as $s) {
			$s = (array) $s;
			
			$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'). Handle tracking '.json_encode($s).'.', 5);
			
			$status = false;
			$enabled = false;
			if (!isset($s['Name'])) {
				continue;
			}
			foreach ($this->STATUS_MAPPING as $STATE) {
				if ($STATE['name'] == $s['Name']) {
					$status = $STATE['status'];
					$enabled = isset($STATE['enabled']) ? $STATE['enabled'] : false;
					break;
				}
			}
			if ($status === false) {
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'). Unknown status: "'.$s['Name'].'".', 0);
				continue;
			}
			if (!$enabled) {
				continue;
			}
			
			$comment = $this->getComment($s, $STATE, $order);

			$q1 = $this->db->query("SELECT * FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order['order_id']."' AND order_status_id='".(int)$status."' AND comment LIKE '".$this->db->escape($comment)."%' ORDER BY date_added DESC LIMIT 1");
			if ($q1->row) {
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'), status: '.$s['Name'].' ('.$status.'). Status already changed.', 3);
				continue;
			}
			
			try {
				$this->model_checkout_order->addOrderHistory($order['order_id'], $status, $comment, $STATE['notify'], true);
				
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'), status: '.$s['Name'].' ('.$status.'). Added order history. '.($STATE['notify'] ? 'Email sent.' : ''), 3);
				if (isset($STATE['smsnotify']) && $STATE['smsnotify']) {
					$this->smsNotify($order, $this->getComment($s, $STATE, $order, 'sms'));
				}
			} catch (Exception $e) {
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'), status: '.$s['Name'].' ('.$status.'). Unable to add order history ('.$e->getMessage().').', 1);
			}
		}
	}

	private function getComment($s, $STATE, $order_info, $key='text') {
		$date = $this->parseDate($s['Date']);
		$text = str_replace('{DATE}', $date, $STATE[$key]);
		$text = str_replace('{COMMENT}', (isset($s['Comment']) ? $s['Comment'] : ''), $text);
		$text = str_replace('{firstname}', $order_info['firstname'], $text);
		$text = str_replace('{lastname}', $order_info['lastname'], $text);
		$text = str_replace('{order_id}', $order_info['order_id'], $text);
		$text = str_replace('{track_no}', $order_info['track_no'], $text);
		$address_arr = array($order_info['shipping_zone'], $order_info['shipping_city'], $order_info['shipping_address_1'], $order_info['shipping_address_2']);
		$address_arr = array_diff($address_arr, array(''));
		$text = str_replace('{address}', implode(', ', $address_arr), $text);
		return $text;
	}

	private function parseDate($str) {
		$date = date_parse(str_replace('T', ' ', $str));
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
	protected function smsNotify($order, $message) {
		$options = array(
			'to'       => $order['telephone'],
			'copy'     => '',
			'from'     => $this->CONFIG['sms_gate_from'],
			'username'    => $this->CONFIG['sms_gate_username'],
			'password' => $this->CONFIG['sms_gate_password'],
			'message'  => $message,
			'ext'      => null
		);
		
		if (!class_exists('Sms')) {
			require_once(DIR_SYSTEM . 'library/sms.php');
		}

		$sms = new Sms($this->config->get('boxberryupd_sms_gatename'), $options);
		$sms->send();
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 4);
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'boxberry_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}

class BoxberryException         extends Exception { }
