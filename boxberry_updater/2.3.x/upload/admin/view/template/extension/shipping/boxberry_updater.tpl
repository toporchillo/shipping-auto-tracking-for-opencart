<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> Редактировать настройки модуля</h3>
      </div>
      <div class="panel-body">
	  
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
				
          <ul class="nav nav-tabs" id="tab_menu">
            <li class="active"><a href="#account" data-toggle="tab">Параметры</a></li>
            <li><a href="#tracking" data-toggle="tab">Уведомления</a></li>
          </ul>
		  
          <div class="tab-content">
            <div class="tab-pane active" id="account">
			
			  <div class="form-group">
				<label class="col-sm-3 control-label" for="input-status">Статус:</label>
				<div class="col-sm-9">
				  <select name="boxberryupd_status" id="input-status" class="form-control">			
					<?php if ($boxberryupd_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  
			  <div class="form-group">
				<label class="col-sm-3 control-label" for="api-token"><span data-toggle="tooltip" title="См. в личном кабинете Boxberry">Boxberry API token:</span></label>
				<div class="col-sm-9">
					<input type="text" id="api-token" name="boxberryupd_token" value="<?php echo $boxberryupd_token; ?>" class="form-control input-small" required />
				</div>
			  </div>
			  
              <div class="form-group">
                <label class="col-sm-3 control-label">Режим работы API:</label>
                <div class="col-sm-9">
                    <select name="boxberryupd_mode" class="form-control">
                    <option value="test"<?php echo ($boxberryupd_mode == 'test' ? ' selected="selected"' : ''); ?>>Тестовый test.api.boxberry.de</option>
                    <option value="old"<?php echo ($boxberryupd_mode == 'old' ? ' selected="selected"' : ''); ?>>Старый api.boxberry.de</option>
                    <option value="new"<?php echo ($boxberryupd_mode == 'new' ? ' selected="selected"' : ''); ?>>Новый api.boxberry.ru</option>
                    </select>                        
                </div>
              </div>
			  
			  <div class="form-group">
				<label class="col-sm-3 control-label" for="shipping-code"><span data-toggle="tooltip" title="См. поле shipping_code в таблице заказов oc_order">Код доставки Boxberry в OpenCart:</span></label>
				<div class="col-sm-9">
					<input type="text" id="shipping-code" name="boxberryupd_shipping_code" value="<?php echo $boxberryupd_shipping_code; ?>" class="form-control input-small" />
				</div>
			  </div>
			  
			  <div class="form-group">
				<label class="col-sm-3 control-label" for="boxberryupd_order_statuses">Не обрабатывать заказы с этими статусами:</label>
				<div class="col-sm-9">
				  <select name="boxberryupd_order_statuses[]" size="15" multiple class="form-control">			
					<?php foreach ($order_statuses as $order_status) { ?>
					<option value="<?php echo $order_status['order_status_id']; ?>"<?php echo (in_array($order_status['order_status_id'], $boxberryupd_order_statuses) ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
					<?php } ?>
				  </select>
				</div>
			  </div>

			  <div class="form-group">
				<label class="col-sm-3 control-label">Команды для планировщика:</label>
				<div class="col-sm-9">
					<b><?php echo $cron_run; ?></b><br>или<br><b><?php echo $wget_run; ?></b>
				</div>
			  </div>
			  
			</div>
		  
            <div class="tab-pane" id="tracking">
			
			  <div class="form-group">
				<label class="col-sm-2 control-label">Подстановка данных в уведомления:</label>
				<div class="col-sm-10">
				  {order_id}&nbsp;-&nbsp;номер заказа, {track_no}&nbsp;-&nbsp;код отслеживания, {firstname},{lastname}&nbsp;-&nbsp;имя и фамилия покупателя, {address}&nbsp;-&nbsp;адрес доставки,
				  {DATE}&nbsp;-&nbsp;дата смены статуса, {COMMENT}&nbsp;-&nbsp;комментарий к статусу.
				</div>
			  </div>
			
			  <div class="form-group">
			  
			  <table class="table table-hover">
				<thead>  
				<tr align="left">
				<th width="20%">Статус заказа в Boxberry</th>
				<th width="10%">Обраб.</th>
				<th width="20%">Статус заказа в OpenCart</th>
				<th width="35%">Писать в историю заказа / Текст SMS-уведомления</th>
				<th width="15%">Уведомлять покупателя</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($status_mapping as $code=>$vals) { ?>
			  
				<tr>
				  <td><?php echo $code.': '.$vals['name'] ?></td>
				  
				  <td>
				  <input type="checkbox" name="<?php echo 'boxberryupd_mapping'.$code.'_enabled';?>" value="1"<?php echo ($vals['enabled'] ? ' checked' : ''); ?>/>
				  </td>
				  
				  <td><select name="<?php echo 'boxberryupd_mapping'.$code.'_status';?>">
						<?php foreach ($order_statuses as $order_status) { ?>
						<?php if ($order_status['order_status_id'] == $vals['status']) { ?>
						<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				  </td>

				  <td>
				  <textarea name="<?php echo 'boxberryupd_mapping'.$code.'_text';?>" rows="3" cols="60"><?php echo $vals['text']; ?></textarea>
				  <?php if (isset($vals['sms'])) { ?>
				  <textarea name="<?php echo 'boxberryupd_mapping'.$code.'_sms';?>" rows="2" cols="60" placeholder="SMS-уведомление"><?php echo $vals['sms']; ?></textarea>
				  <?php } ?>
				  </td>
				  
				  <td>
				  <label><input type="checkbox" name="<?php echo 'boxberryupd_mapping'.$code.'_notify';?>" value="1"<?php echo ($vals['notify'] ? ' checked' : ''); ?>/>Email</label>
				  <?php if (isset($vals['sms'])) { ?>
				  <br><br>
				  <label><input type="checkbox" name="<?php echo 'boxberryupd_mapping'.$code.'_smsnotify';?>" value="1"<?php echo ($vals['smsnotify'] ? ' checked' : ''); ?>/>SMS</label>
				  <?php } ?>
				  </td>
				  
				  <?php } ?>
				<tbody>
			  </table>
			  </div>
			  
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-template">SMS шлюз:</label>
                  <div class="col-sm-9">
                    <select name="boxberryupd_sms_gatename" id="select-smsgate" class="form-control">
                      <?php foreach ($sms_gatenames as $sms_gatename) { ?>
                      <?php if ($boxberryupd_sms_gatename == $sms_gatename) { ?>
                      <option value="<?php echo $sms_gatename; ?>" selected="selected"><?php echo $sms_gatename; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $sms_gatename; ?>"><?php echo $sms_gatename; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                    <span id="sms_gate_link"></span>
				  </div>
				</div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-sms-gate-username">Логин или ID на SMS шлюзе:</label>
                  <div class="col-sm-9">
                    <input type="text" name="boxberryupd_sms_gate_username" value="<?php echo $boxberryupd_sms_gate_username; ?>" id="input-sms-gate-username" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-config-sms-gate-password">Пароль,token или API key на SMS шлюзе:</label>
                  <div class="col-sm-9">
                    <input type="password" name="boxberryupd_sms_gate_password" value="<?php echo $boxberryupd_sms_gate_password; ?>" id="input-sms-gate-password" class="form-control" />
                  </div>
				</div>	
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="input-config-sms-gate-from">Отправитель SMS:</label>
                  <div class="col-sm-9">
                    <input type="text" name="boxberryupd_sms_gate_from" value="<?php echo $boxberryupd_sms_gate_from; ?>" id="input-sms-gate-from" class="form-control" />
                  </div>
				</div>	
			  
			</div>
			
        </div>
		  </div>
		  <input type="hidden" name="boxberryupd_set" value="1" />
		</form>
      </div>
	</div>
  </div>
</div>
<script>
var smsGates = {
    bytehand: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
    bytehandcom: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
	smsc: 'https://smsc.ru/?ppOpenCart',
    smscru: 'https://smsc.ru/?ppOpenCart',
    smscua: 'https://smsc.ru/?ppOpenCart',
	epochtasms: 'http://www.epochta.ru/#a_aid=opencart',
	epochta: 'http://www.epochta.ru/#a_aid=opencart',
    epochtasmscomua: 'http://www.epochta.ru/#a_aid=opencart',
	unisender: 'http://www.unisender.com/?a=opencart',
    unisenderru: 'http://www.unisender.com/?a=opencart'
};
$('#select-smsgate').change(function() {
	var gate = $(this).val();
	if (smsGates[gate]) {
		$('#sms_gate_link').html('<a href="'+smsGates[gate]+'" target="_blank">Получить доступ</a>');
	}
	else {
		$('#sms_gate_link').html('');
	}
});
$('#select-smsgate').trigger('change');

</script>
<?php echo $footer; ?>