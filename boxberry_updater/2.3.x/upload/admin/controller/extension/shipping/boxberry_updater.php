<?php
/**
* Boxberry shipping tracking for OpenCart (ocStore) 2.3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: ...
*/
class ControllerExtensionShippingBoxberryUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'status'=>true,
		'token' => '',
        'mode'=>'test',
		'shipping_code'=>'',
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);
	private $last_error = '';

	public $STATUS_MAPPING = array(
	 1 => array('name'=>'Принято к доставке',
		'notify'=>true, 'enabled'=>true, 'status'=>2, 'text'=>'{firstname}, ваш заказ #{order_id} зарегистрирован в базе данных Boxberry, вы можете отслеживать заказ на сайте: http://www.boxberry.ru/ru/tracking/',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте www.boxberry.ru, код отслеживания: {track_no}'),
	 2 => array('name'=>'Передано на сортировку',
		'notify'=>false, 'enabled'=>true, 'status'=>2, 'text'=>'Заказ передан на пункт сортировки Boxberry.'),
	 3 => array('name'=>'Отправлено в город назначения',
		'notify'=>false, 'enabled'=>true, 'status'=>2, 'text'=>'Заказ отправлен в город назначения.'),
	 4 => array('name'=>'Поступило в пункт выдачи',
		'notify'=>true, 'enabled'=>true, 'status'=>3, 'text'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его в пункте выдачи Boxberry. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} доставлен. Вы можете забрать его в пункте выдачи Boxberry.  {COMMENT}'),
	 5 => array('name'=>'Передано на курьерскую доставку',
		'notify'=>true, 'enabled'=>true, 'status'=>2, 'text'=>'{firstname}, ваш заказ #{order_id} передан курьеру на доставку. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} передан курьеру на доставку. {COMMENT}'),
	 6 => array('name'=>'Выдано',
		'notify'=>false, 'enabled'=>true, 'status'=>3, 'text'=>'Заказ доставлен и вручен адресату. {COMMENT}'),
	 7 => array('name'=>'Готовится к возврату',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'{firstname}, вы не забрали заказ в пункте выдачи Boxberry. Заказ готовится к возврату. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, вы не забрали заказ #{order_id} в пункте выдачи Boxberry. Заказ готовится к возврату. {COMMENT}'),
	 8 => array('name'=>'Возвращено с курьерской доставки',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'{firstname}, курьер не смог вручить вам заказ #{order_id}. Заказ готовится к возврату. {COMMENT}',
		'smsnotify'=>true, 'sms'=>'{firstname}, курьер не смог вручить вам заказ #{order_id}. Заказ готовится к возврату. {COMMENT}'),
	 9 => array('name'=>'Отправлено в пункт приема',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'Покупатель отказался от покупки, заказ отправлен в ИМ. {COMMENT}'),
	10 => array('name'=>'Возвращено в пункт приема',
		'notify'=>true, 'enabled'=>true, 'status'=>8, 'text'=>'Недоставленный заказ вернулся в пункт приема. {COMMENT}'),
	11 => array('name'=>'Возвращено в ИМ',
		'notify'=>true, 'enabled'=>true, 'status'=>13, 'text'=>'Заказ возвращен в ИМ. {COMMENT}')
	);

	private function setConfig() {
		if (!$this->config->get('boxberryupd_set')) {
			foreach($this->STATUS_MAPPING as $code=>$vals) {
				$this->STATUS_MAPPING[$code]['text'] = "{DATE}\n".$vals['text'];
			}
			return;
		}
	
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('boxberryupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('boxberry_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('boxberryupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('boxberryupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('boxberryupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('boxberryupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('boxberryupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('boxberryupd_mapping'.$code.'_smsnotify');
			}
		}
	}

	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='boxberry_updater'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='boxberryupd'");
	}
	
	public function index() {
		$this->load->language('extension/shipping/boxberry_updater');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['boxberryupd_order_statuses'] = implode(',', $this->request->post['boxberryupd_order_statuses']);
			
			$this->model_setting_setting->editSetting('boxberryupd', $this->request->post);	
			$module_status = array('boxberry_updater_status'=>$this->request->post['boxberryupd_status']);
			$this->model_setting_setting->editSetting('boxberry_updater', $module_status);	
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
			return;
		}
		
		$this->setConfig();
		
 		if (isset($this->last_error['warning'])) {
			$data['error_warning'] = $this->last_error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);
    
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/boxberry_updater', 'token=' . $this->session->data['token'], true)
		);
		
		$data['action'] = $this->url->link('extension/shipping/boxberry_updater', 'token=' . $this->session->data['token'], true);
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);
    
		$data['token'] = $this->session->data['token'];
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['cron_run'] = 'php '.realpath(DIR_CATALOG.'../boxberry_updater.php');
		$data['wget_run'] = 'wget -O - '.HTTPS_CATALOG.'index.php?route=api/boxberry_updater/update';

		$this->load->model('localisation/order_status');
    	$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['boxberryupd_order_statuses'])) {
			$data['boxberryupd_order_statuses'] = $this->request->post['boxberryupd_order_statuses'];
		} else {
			$data['boxberryupd_order_statuses'] = explode(',', $this->config->get('boxberryupd_order_statuses'));
		}

		$data['sms_gatenames'] = array();
		$files = glob(DIR_SYSTEM . 'smsgate/*.php');
		foreach ($files as $file) {
			$data['sms_gatenames'][] =  basename($file, '.php');
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['boxberryupd_'.$key])) {
				$data['boxberryupd_'.$key] = $this->request->post['boxberryupd_'.$key];
			} else {
				$data['boxberryupd_'.$key] = $conf;
			}
		}
		
		$data['status_mapping'] = $this->STATUS_MAPPING;

		if (isset($this->request->post['boxberry_updater_status'])) {
			$data['boxberry_updater_status'] = $this->request->post['boxberry_updater_status'];
		} else {
			$data['boxberry_updater_status'] = $this->CONFIG['status'];
		}
		
		foreach($this->STATUS_MAPPING as $code=>$val) {
			if (isset($this->request->post['boxberryupd_mapping'.$code.'_notify'])) {
				$data['boxberryupd_mapping'.$code.'_notify'] = $this->request->post['boxberryupd_mapping'.$code.'_notify'];
			} else {
				$data['boxberryupd_mapping'.$code.'_notify'] = $val['notify'];
			}
			if (isset($this->request->post['boxberryupd_mapping'.$code.'_smsnotify'])) {
				$data['boxberryupd_mapping'.$code.'_smsnotify'] = $this->request->post['boxberryupd_mapping'.$code.'_smsnotify'];
			} elseif (isset($val['smsnotify'])) {
				$data['boxberryupd_mapping'.$code.'_smsnotify'] = $val['smsnotify'];
			}
			
			if (isset($this->request->post['boxberryupd_mapping'.$code.'_enabled'])) {
				$data['boxberryupd_mapping'.$code.'_enabled'] = $this->request->post['boxberryupd_mapping'.$code.'_enabled'];
			} else {
				$data['boxberryupd_mapping'.$code.'_enabled'] = $val['enabled'];
			}
			
			if (isset($this->request->post['boxberryupd_mapping'.$code.'_status'])) {
				$data['boxberryupd_mapping'.$code.'_status'] = $this->request->post['boxberryupd_mapping'.$code.'_status'];
			} else {
				$data['boxberryupd_mapping'.$code.'_status'] = $val['status'];
			}
			
			if (isset($this->request->post['boxberryupd_mapping'.$code.'_text'])) {
				$data['boxberryupd_mapping'.$code.'_text'] = $this->request->post['boxberryupd_mapping'.$code.'_text'];
			} else {
				$data['boxberryupd_mapping'.$code.'_text'] = $val['text'];
			}

			if (isset($this->request->post['boxberryupd_mapping'.$code.'_sms'])) {
				$data['boxberryupd_mapping'.$code.'_sms'] = $this->request->post['boxberryupd_mapping'.$code.'_sms'];
			} elseif (isset($val['sms'])) {
				$data['boxberryupd_mapping'.$code.'_sms'] = $val['sms'];
			}
		}
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
				
		$this->response->setOutput($this->load->view('extension/shipping/boxberry_updater.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/boxberry_updater')) {
			$this->last_error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->last_error) {
			return true;
		} else {
			return false;
		}	
	}
}
