<?php
/**
 * Kazpost tracking API PHP library
 * @author Alexander Toporkov <toporchillo@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$kazpostRequiredExtensions = array('SimpleXML', 'curl', 'pcre');
foreach($kazpostRequiredExtensions as $russianpostExt) {
	if (!extension_loaded($russianpostExt)) {
		throw new KazpostSystemException('Для отслеживания отправлений Казпочты надо установить PHP-расширение ' . $russianpostExt);
	}
}

class KazpostAPI {
  /**
   * Service URL
   */
  const Endpoint = 'http://track.kazpost.kz/api/v2/';

  protected $proxyHost;
  protected $proxyPort;
  protected $proxyAuthUser;
  protected $proxyAuthPassword;

  /**
   * Constructor. Pass proxy config here.
   * @param string $proxyHost
   * @param string $proxyPort
   * @param string $proxyAuthUser
   * @param string $proxyAuthPassword
   */
  public function __construct($proxyHost = "", $proxyPort = "", $proxyAuthUser = "", $proxyAuthPassword = "") {
    $this->proxyHost         = $proxyHost;
    $this->proxyPort         = $proxyPort;
    $this->proxyAuthUser     = $proxyAuthUser;
    $this->proxyAuthPassword = $proxyAuthPassword;
  }

  /**
   * Returns tracking data
   * @param string $trackingNumber tracking number
   * @return array
   */
  public function getOperationHistory($trackingNumber) {
    $trackingNumber = trim($trackingNumber);
    if (!preg_match('/^[A-Z]{2}[0-9]{9}[A-Z]{2}$/', $trackingNumber)) {
		throw new RussianPostArgumentException('Неверный формат трек-номера: ' . $trackingNumber);
    }
	
    $data = $this->makeRequest($trackingNumber);
    $data = $this->parseResponse($data);
	$result = array();
	foreach (array_reverse($data['events']) as $day) {
		foreach($day['activity'] as $event) {
			$event['date'] = $day['date'];
			$result[] = $event;
		}
	}
    return $result;
  }

  protected function parseResponse($raw) {
    try {
        $data = json_decode($raw, true);
    }
    catch (Exception $e) {
	   throw new KazpostDataException("Сервис вернул невалидный JSON");
	}
	if (!$data) {
	   throw new KazpostDataException("Сервис вернул невалидный JSON");
	}
    if (isset($data['error']) && $data['error']) {
	   throw new KazpostDataException($data['error']);
    }
    return $data;
  }

  protected function makeRequest($trackingNumber) {
    $channel = curl_init(self::Endpoint . urlencode($trackingNumber) . '/events');

    curl_setopt_array($channel, array(
        CURLOPT_SSL_VERIFYPEER, false,
        CURLOPT_SSL_VERIFYHOST, false,
        CURLOPT_POST           => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CONNECTTIMEOUT => 5,
        CURLOPT_TIMEOUT        => 10,
    ));

    if (!empty($this->proxyHost) && !empty($this->proxyPort)) {
      curl_setopt($channel, CURLOPT_PROXY, $this->proxyHost . ':' . $this->proxyPort);
    }

    if (!empty($this->proxyAuthUser)) {
      curl_setopt($channel, CURLOPT_PROXYUSERPWD, $this->proxyAuthUser . ':' . $this->proxyAuthPassword);
    }

    $result = curl_exec($channel);
    if ($errorCode = curl_errno($channel)) {
        throw new KazpostChannelException(curl_error($channel), $errorCode);
    }
    return $result;    
  }
}

class KazpostException         extends Exception { }
class KazpostArgumentException extends KazpostException { }
class KazpostSystemException   extends KazpostException { }
class KazpostChannelException  extends KazpostException { }
class KazpostDataException     extends KazpostException { }

class KazpostStatus {
	public $statuses = array();
	
	public function __construct() {
		$json = trim(file_get_contents(dirname(__FILE__).'/kazpost.status.json'));
		$this->statuses = json_decode($json, true);
	}
	
	public function getStatus($code) {
		if (isset($this->statuses[$code])) {
			return $this->statuses[$code];
		}
		return array();
	}
}