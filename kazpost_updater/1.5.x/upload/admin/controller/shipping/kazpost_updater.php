<?php
/**
* Kazpost tracking for OpenCart (ocStore) 1.5.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
*/
class ControllerShippingKazpostUpdater extends Controller {
	protected $error = array();
	
	public $ECHO_LOG = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'corporate'=>0,
		'period'=>0.1,
		'Timezone_diff'=>4,
		//'shipping_code'=>'',
		'delivery_status'=>2,
		'returning_status'=>8,
		'delivered_status'=>3,
		'returned_status'=>13,
		'ok_status'=>5,
		'fail_status'=>10,
		'skip_inessential'=>0,
		
		'start_text'=>"Уважаемый {shipping_firstname}, ваш заказ №{order_id} передан Казпочте, отделение почтовой связи '{WHERE}'. Код почтового отправления: {track_no}.",
		'start_notify'=>1,
		'start_sms'=>"{shipping_firstname}, ваш заказ #{order_id} передан Казпочте '{WHERE}'. Код отправления: {track_no}.",
		'start_sms_notify'=>1,
		'delivered_text'=>"Уважаемый {shipping_firstname}, ваш заказ №{order_id} прибыл в отделение почтовой связи {INDEX} ({WHERE}). Код почтового отправления: {track_no}.",
		'delivered_notify'=>1,
		'delivered_sms'=>"{shipping_firstname}, ваш заказ #{order_id} прибыл в почтовое отделение {INDEX} ({WHERE}). Код отправления: {track_no}.",
		'delivered_sms_notify'=>1,
		//'repeat_text'=>"Уважаемый {shipping_firstname}, ваш заказ №{order_id} уже давно прибыл в отделение почтовой связи {INDEX} ({WHERE}). Код почтового отправления: {track_no}.",
		//'repeat_notify'=>1,
		//'repeat_sms'=>"{shipping_firstname}, ваш заказ #{order_id} уже давно прибыл в почтовое отделение {INDEX} ({WHERE}). Код отправления: {track_no}.",
		//'repeat_sms_notify'=>1,
		//'repeat_notify_days'=>0,
		'returning_text'=>"Уважаемый {shipping_firstname}, заказ №{order_id} не был вами получен в отделении почтовой связи '{WHERE}'. Почта начала возврат заказа отправителю.",
		'returning_notify'=>1,
		'ok_text'=>"Уважаемый {shipping_firstname}, спасибо за покупку в нашем магазине.",
		'ok_notify'=>0,
		
		'text'=>"изменен статус почтового отправления. Новый статус: {WHERE}, {STATUS}",
		'notify'=>1
	);

	private function setConfig() {
		if ($this->config->get('kazpost_updater_set')) {
			foreach($this->CONFIG as $key=>$conf) {
				$this->CONFIG[$key] = $this->config->get('kazpost_updater_'.$key);
			}
		}
	}
	
	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `group`='kazpost_updater'");
	}
	
	public function index() {
		$this->load->language('shipping/kazpost_updater');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['kazpost_updater_order_statuses'] = implode(',', $this->request->post['kazpost_updater_order_statuses']);
			/*
			$this->request->post['kazpost_updater_repeat_notify_days'] = intval($this->request->post['kazpost_updater_repeat_notify_days']);
			if (($this->request->post['kazpost_updater_repeat_notify'] || $this->request->post['kazpost_updater_repeat_sms_notify']) && !intval($this->request->post['kazpost_updater_repeat_notify_days'])) {
				$this->request->post['kazpost_updater_repeat_notify_days'] = 3;
			}
			*/
			$this->model_setting_setting->editSetting('kazpost_updater', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
			return;
		}
		
		$this->setConfig();
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/kazpost_updater', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/kazpost_updater', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 		
		
		
		$this->load->model('localisation/order_status');
    	$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		if (isset($this->request->post['kazpost_updater_status'])) {
			$this->data['kazpost_updater_status'] = $this->request->post['kazpost_updater_status'];
		} else {
			$this->data['kazpost_updater_status'] = $this->config->get('kazpost_updater_status');
		}

		if (isset($this->request->post['kazpost_updater_order_statuses'])) {
			$this->data['kazpost_updater_order_statuses'] = $this->request->post['kazpost_updater_order_statuses'];
		} else {
			$this->data['kazpost_updater_order_statuses'] = explode(',', $this->config->get('kazpost_updater_order_statuses'));
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['kazpost_updater_'.$key])) {
				$this->data['kazpost_updater_'.$key] = $this->request->post['kazpost_updater_'.$key];
			} else {
				$this->data['kazpost_updater_'.$key] = $conf;
			}
		}

		$this->template = 'shipping/kazpost_updater.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/kazpost_updater')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		elseif (!isset($this->request->post['kazpost_updater_order_statuses'])) {
			$this->error['warning'] = 'Укажите один или несколько статусов заказа, которые не надо отслеживать.';
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private $kazpost_status;
	
	public function update() {
		$this->setConfig();
    	$this->language->load('sale/order');
		$this->load->model('sale/order');
		
		if (isset($_SERVER['HTTP_HOST']) && $this->ECHO_LOG) {
			header("Content-Type: text/html; charset=utf-8");
		}
		$this->log('Начинаем отслеживать отправления Казпочты...', 3);
		if (!$this->config->get('kazpost_updater_status')) {
			$this->log('Модуль "Автотреккинг доставок Казпочты" отключен', 3);
			return false;
		}
		
		//include the library
		require_once(DIR_SYSTEM . 'library/kazpost.lib.php');
		
		$this->kazpost_status = new KazpostStatus();
		
		$limit = 3000;
		$orders = $this->getOrdersToUpdate($limit);
		$c = 0;
		$i = 0;
		while ($i<count($orders)) {
			$order = $orders[$i];
			$i++;
			if (!preg_match('/^[A-Z]{2}[0-9]{9}[A-Z]{2}$/', $order['track_no'])) {
				continue;
			}
			
			try {
				//init the client
				$this->log('Запрос к API Казпочты', 3);
				$client = new KazpostAPI();
				//fetch info
				$state = $client->getOperationHistory($order['track_no']);
				
				$query = $this->db->query("SELECT comment, date_added FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order['order_id']."' ORDER BY order_history_id DESC");
				$this->handleStatus($order, $state, $query->rows);
			} catch(KazpostException $e) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].') Ошибка сервиса Казпочты: ' . $e->getMessage(), 1);
			}
		}
		$this->log('Отслеживание отправлений Казпочты завершено.', 3);
	}
	
	protected function getOrdersToUpdate($limit) {
		$not_in = ($this->config->get('kazpost_updater_order_statuses') ? $this->config->get('kazpost_updater_order_statuses') : '0');
		$query = $this->db->query("SELECT o.*
			FROM `" . DB_PREFIX . "order` o
			WHERE o.track_no <> '' AND o.order_status_id <> '0' AND NOT(o.order_status_id IN($not_in)) ORDER BY o.order_id DESC LIMIT $limit");
		return $query->rows;
	}
	
	private function parseDate($str) {
		$date = date_parse($str);
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
	private $received_stats = array('BAT', 'PRC', 'RCP', 'RCPOPS');
	private $issued_stats = array('DPAY', 'ISSPAY', 'ISSSC', 'BOXISS');
	private $notissued_stats = array('NON', 'NON_S');
	private $delivered_stats = array('DLV', 'DLV_POBOX', 'TRNPST');
	private $return_stats = array('RET','RETSC','RETSCSTR','RETPST');
	
	private function handleStatus($order, $state, $comments) {
		$return = false;
		$delivered = false;
		foreach ($state as $j=>$s) {
			$s = (array) $s;
			
			$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Обрабатываем данные '.json_encode($s).'.', 5);

			//Возвращается
			if (count(array_intersect($s['status'], $this->return_stats)) > 0) {
				$return = true;
			}
			//Отсекаем старые треккинги 2-летней давности
			/*
			Возможно потребуется в будущем
			*/
			
			$already_added = false;
			foreach($comments as $i=>$row) {
				if (strpos($row['comment'], $s['date'].' '.$s['time']) === 0) {
					$already_added = true;
				}
			}
			/*
			$repeat_notify = false;
			if (($this->CONFIG['repeat_notify'] || $this->CONFIG['repeat_sms_notify']) && $delivered && !$return && ($j == count($state)-1 || $j == (count($state)-2)) 
				&& ((time() - strtotime($comments[0]['date_added'])) >= 3600*24*intval($this->CONFIG['repeat_notify_days']))) {
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Повторно уведомляем покупателя о прибытии заказа.', 3);
					$already_added = false;
					$repeat_notify = true;
			}
			*/
			if ($already_added) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Данные уже обработаны ранее.', 4);
				continue;
			}
			
			$notify = false;
			 //Принята почтой
			if (count(array_intersect($s['status'], $this->received_stats))>0) {
				$status = $this->CONFIG['delivery_status'];
				$notify = $this->CONFIG['start_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['start_text']);
				if ($this->CONFIG['start_sms_notify']) {
					$msg = $this->getComment($order, $s, $this->CONFIG['start_sms']);
					$this->smsNotify($order, $msg);
				}
			}
			 //Доставлена
			elseif (!$return && count(array_intersect($s['status'], $this->delivered_stats))>0) {
				$delivered = true;
				$status = $this->CONFIG['delivered_status'];
				//if (!$repeat_notify) {
					$notify = $this->CONFIG['delivered_notify'];
					$notify_text = $this->getComment($order, $s, $this->CONFIG['delivered_text']);
					if ($this->CONFIG['delivered_sms_notify']) {
						$msg = $this->getComment($order, $s, $this->CONFIG['delivered_sms']);
						$this->smsNotify($order, $msg);
					}
				/*
				}
				else {
					$notify = $this->CONFIG['repeat_notify'];
					$notify_text = $this->getComment($order, $s, $this->CONFIG['repeat_text']);
					if ($this->CONFIG['repeat_sms_notify']) {
						$msg = $this->getComment($order, $s, $this->CONFIG['repeat_sms']);
						$this->smsNotify($order, $msg);
					}
				}
				*/
			}
			 //Возвращается
			elseif (count(array_intersect($s['status'], $this->return_stats))>0) {
				$delivered = false;
				$status = $this->CONFIG['returning_status'];
				$notify = $this->CONFIG['returning_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['returning_text']);
			}
			 //Вручена
			elseif (!$return && count(array_intersect($s['status'], $this->issued_stats))>0) {
				$delivered = false;
				$status = $this->CONFIG['ok_status'];
				$notify = $this->CONFIG['ok_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['ok_text']);
			}
			 //Вернулась
			elseif ($return && count(array_intersect($s['status'], $this->delivered_stats))>0) {
				$delivered = false;
				$status = $this->CONFIG['returned_status'];
			}
			 //Вручен отправителю
			elseif ($return && count(array_intersect($s['status'], $this->issued_stats))>0) {
				$delivered = false;
				$status = $this->CONFIG['fail_status'];
			}
			else {
				$status = $delivered ? $this->CONFIG['delivered_status'] 
					: ($return ? $this->CONFIG['returning_status'] : $this->CONFIG['delivery_status']);
				if ($this->CONFIG['skip_inessential']) {
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Пропускаем несущественный статус: '.$s['date'].' '.$s['time'].', '.$s['zip'].' '.$s['city'].' - '.implode(',', $s['status']), 4);
					continue;
				}
			}
		
			try {
				$comment = $this->getComment($order, $s, $s['date'].' '.$s['time'].' '.$this->CONFIG['text']);
				$data = array(
					'order_status_id' => $status,
					'notify' => false,
					'comment' => $comment
				);
				$this->model_sale_order->addOrderHistory($order['order_id'], $data);
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Добавлена история заказа: '.$comment, 3);
				if ($notify) {
					$data = array(
						'order_status_id' => $status,
						'notify' => true,
						'comment' => $notify_text
					);
					$this->model_sale_order->addOrderHistory($order['order_id'], $data);
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Покупатель уведомлен: '.$notify_text, 3);
				}
			} catch (Exception $e) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Ошибка добавления истории заказа ('.$e->getMessage().').', 1);
			}
		}
	}

	protected function smsNotify($order, $message) {
		if ($this->config->get('config_sms_alert')) {
			$options = array(
				'to'       => $order['telephone'],
				'copy'     => '',
				'from'     => $this->config->get('config_sms_from'),
				'username'    => $this->config->get('config_sms_gate_username'),
				'password' => $this->config->get('config_sms_gate_password'),
				'message'  => $message,
				'ext'      => null
			);
			
			$this->load->library('sms');

			$sms = new Sms($this->config->get('config_sms_gatename'), $options);
			$sms->send();
			$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 3);
		}
	}
	
	private function getComment($order, $state, $text) {
		foreach ($order as $key=>$val) {
			$text = str_replace('{'.$key.'}', $val, $text);
		}
		$text = str_replace('{WHERE}', $state['city'].', '.$state['name'], $text);
		$text = str_replace('{INDEX}', $state['zip'], $text);
		
		if (strpos($text, '{STATUS}') !== false) {
			$stat_texts = array();
			foreach ($state['status'] as $code) {
				$full_status = $this->kazpost_status->getStatus($code);
				if (isset($full_status['title_ru'])) {
					$stat_texts[] = $full_status['title_ru'];
				}
			}
			$text = str_replace('{STATUS}', implode(', ', $stat_texts), $text);
		}
		return $text;
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'kazpost_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO_LOG) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}
