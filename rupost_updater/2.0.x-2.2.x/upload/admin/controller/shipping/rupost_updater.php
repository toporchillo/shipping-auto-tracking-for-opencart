<?php
/**
* Russian post tracking for OpenCart (ocStore) 2.0.x - 2.2.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: http://liveopencart.ru/opencart-moduli-shablony/moduli/dostavka/avtomaticheskoe-otslejivanie-pochtovyih-otpravleniy-pochtyi-rossii
*/
//include the library
require_once(DIR_SYSTEM . 'library/russianpost.lib.php');

class ControllerShippingRupostUpdater extends Controller {
	protected $error = array();
	
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'login'=>'',
		'password'=>'',
		'corporate'=>0,
		'assign'=>0,
		'apikey'=>'',
		'token'=>'',
		'period'=>0.1,
		'Timezone_diff'=>4,
		'shipping_code'=>'',
		'last_hours'=>-1,
		'assign_status'=>2,
		'delivery_status'=>2,
		'returning_status'=>8,
		'delivered_status'=>3,
		'returned_status'=>13,
		'ok_status'=>5,
		'fail_status'=>10,
		'skip_inessential'=>0,
		
		'assign_text'=>"{firstname}, вашему заказу №{order_id} присвоен код почтового отправления: {track_no}.",
		'assign_notify'=>1,
		'assign_sms'=>"{firstname}, вашему заказ #{order_id} присвоен код отправления: {track_no}.",
		'assign_sms_notify'=>1,
		'start_text'=>"{firstname}, ваш заказ №{order_id} передан почте России, отделение почтовой связи '{WHERE}'. Код почтового отправления: {track_no}.",
		'start_notify'=>1,
		'start_sms'=>"{firstname}, ваш заказ #{order_id} передан почте России '{WHERE}'. Код отправления: {track_no}.",
		'start_sms_notify'=>1,
		'delivered_text'=>"{firstname}, ваш заказ №{order_id} прибыл в отделение почтовой связи '{WHERE}'. Код почтового отправления: {track_no}.",
		'delivered_notify'=>1,
		'delivered_sms'=>"{firstname}, ваш заказ #{order_id} прибыл в почтовое отделение '{WHERE}'. Код отправления: {track_no}.",
		'delivered_sms_notify'=>1,
		'repeat_text'=>"{firstname}, ваш заказ №{order_id} уже давно прибыл в отделение почтовой связи {INDEX} ({WHERE}). Код почтового отправления: {track_no}.",
		'repeat_notify'=>1,
		'repeat_sms'=>"{firstname}, ваш заказ #{order_id} уже давно прибыл в почтовое отделение {INDEX} ({WHERE}). Код отправления: {track_no}.",
		'repeat_sms_notify'=>1,
		'repeat_notify_days'=>0,
		'returning_text'=>"{firstname}, заказ №{order_id} не был вами получен в отделении почтовой связи '{WHERE}'. Почта начала возврат заказа отправителю.",
		'returning_notify'=>1,
		'ok_text'=>"{firstname}, спасибо за покупку в нашем магазине.",
		'ok_notify'=>0,
        'ok_reward'=>0,
		
		'text'=>"изменен статус почтового отправления. Новый статус: {WHERE}, {STATUS}",
		'notify'=>1,
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);

	private function setConfig() {
		if ($this->config->get('rupostupd_set')) {
			foreach($this->CONFIG as $key=>$conf) {
				$this->CONFIG[$key] = $this->config->get('rupostupd_'.$key);
			}
		}
	}
	
	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='rupostupd' OR `code`='rupost_updater'");
	}
	
	public function index() {
		$this->load->language('shipping/rupost_updater');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->request->post['rupostupd_order_statuses'] = implode(',', $this->request->post['rupostupd_order_statuses']);
			$this->request->post['rupostupd_repeat_notify_days'] = intval($this->request->post['rupostupd_repeat_notify_days']);
			if ((isset($this->request->post['rupostupd_repeat_notify']) || isset($this->request->post['rupostupd_repeat_sms_notify'])) && !intval($this->request->post['rupostupd_repeat_notify_days'])) {
				$this->request->post['rupostupd_repeat_notify_days'] = 3;
			}
			$this->model_setting_setting->editSetting('rupostupd', $this->request->post);	
			$module_status = array('rupost_updater_status'=>$this->request->post['rupostupd_status']);
			$this->model_setting_setting->editSetting('rupost_updater', $module_status);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
			return;
		}
		$data = array();
		$this->setConfig();
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/rupost_updater', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('shipping/rupost_updater', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 		
		
		
		$this->load->model('localisation/order_status');
    	$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		if (isset($this->request->post['rupostupd_status'])) {
			$data['rupostupd_status'] = $this->request->post['rupostupd_status'];
		} else {
			$data['rupostupd_status'] = $this->config->get('rupostupd_status');
		}
		
		if (isset($this->request->post['rupostupd_order_statuses'])) {
			$data['rupostupd_order_statuses'] = $this->request->post['rupostupd_order_statuses'];
		} else {
			$data['rupostupd_order_statuses'] = explode(',', $this->config->get('rupostupd_order_statuses'));
		}

		$data['sms_gatenames'] = array();
		$files = glob(DIR_SYSTEM . 'smsgate/*.php');
		foreach ($files as $file) {
			$data['sms_gatenames'][] =  basename($file, '.php');
		}
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['rupostupd_'.$key])) {
				$data['rupostupd_'.$key] = $this->request->post['rupostupd_'.$key];
			} else {
				$data['rupostupd_'.$key] = $conf;
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('shipping/rupost_updater.tpl', $data));		
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/rupost_updater')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		else if (!isset($this->request->post['rupostupd_order_statuses'])) {
			$this->error['warning'] = 'Укажите один или несколько статусов заказа, которые не надо отслеживать.';
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
