<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" class="vtabs-content">
          <table class="form">
		  
            <tr>
              <td>Статус:</td>
              <td><select name="rupost_updater_status">
                  <?php if (${'rupost_updater_status'}) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td>Доступ к API Почты России:</td>
              <td>
			 <?php foreach ($rupostupd_logins as $i=>$login) { ?>
			  <div class="api_login_password">
				Логин: <input type="text" name="rupostupd_logins[]" value="<?php echo $login; ?>" size="16" required />
				&nbsp;Пароль: <input type="password" name="rupostupd_passwords[]" value="<?php echo $rupostupd_passwords[$i]; ?>" size="16" required />
			  <img src="view/image/delete.png" class="_remove_login_password" style="cursor: pointer; float: right;" />
			  </div>
			  <?php } ?>
			  <img src="view/image/add.png" id="_add_login_password" style="cursor: pointer;  float: right;" />
			  <a href="https://tracking.pochta.ru/specification" target="_blank">зарегистрироваться</a>
              </td>
            </tr>
            <tr>
              <td>Корпоративный клиент:<br/><span class="help">Для клиентов без договора лимит доступа к серверу Почты России - 100 запросов в сутки.</span></td>
              <td>
			  <input type="checkbox" name="rupostupd_corporate" value="1" <?php echo ($rupostupd_corporate ? ' checked="checked"' : ''); ?>/>
              </td>
            </tr>
            <tr>
              <td>Код доставки Почты России в OpenCart:<br/><span class="help">Если вы не отправляете заказы другими способами доставки, можно оставить это поле пустым.</span></td>
              <td>
			  <input type="text" name="rupostupd_shipping_code" value="<?php echo $rupostupd_shipping_code; ?>" size="16" />
              </td>
            </tr>
			<tr>
			  <td>Не обрабатывать заказы с этими статусами:</td>
			  <td><select name="rupostupd_order_statuses[]" size="15" multiple required>
				  <?php foreach ($order_statuses as $order_status) { ?>
				  <option value="<?php echo $order_status['order_status_id']; ?>"<?php echo (in_array($order_status['order_status_id'], $rupostupd_order_statuses) ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			
            <tr>
              <td>Статус заказа, когда он доставляется покупателю:</td>
              <td><select name="rupostupd_delivery_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_delivery_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>
            <tr>
              <td>Текст уведомления покупателя о начале доставки:</td>
              <td><textarea name="rupostupd_start_text" rows="6" cols="100"><?php echo $rupostupd_start_text; ?></textarea>
				<br/><label for="start_notify_cb"><input type="checkbox" name="rupostupd_start_notify" value="1" id="start_notify_cb" <?php echo ($rupostupd_start_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label>
			  </td>
            </tr>
            <tr>
              <td>Текст SMS-уведомления покупателя о начале доставки:</td>
              <td><textarea name="rupostupd_start_sms" rows="4" cols="100"><?php echo $rupostupd_start_sms; ?></textarea>
				<br/><label for="start_sms_notify_cb"><input type="checkbox" name="rupostupd_start_sms_notify" value="1" id="start_sms_notify_cb" <?php echo ($rupostupd_start_sms_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя по SMS</label>
			  </td>
            <tr>
			
            <tr>
              <td>Статус заказа, когда он прибыл в место вручения:</td>
              <td><select name="rupostupd_delivered_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_delivered_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>
            <tr>
              <td>Текст уведомления покупателя о прибытии:</td>
              <td><textarea name="rupostupd_delivered_text" rows="6" cols="100"><?php echo $rupostupd_delivered_text; ?></textarea>
				<br/><label for="delivered_notify_cb"><input type="checkbox" name="rupostupd_delivered_notify" value="1" id="delivered_notify_cb" <?php echo ($rupostupd_delivered_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label>
			  </td>
            </tr>
            <tr>
              <td>Текст SMS-уведомления покупателя о прибытии:</td>
              <td><textarea name="rupostupd_delivered_sms" rows="4" cols="100"><?php echo $rupostupd_delivered_sms; ?></textarea>
				<br/><label for="delivered_sms_notify_cb"><input type="checkbox" name="rupostupd_delivered_sms_notify" value="1" id="delivered_sms_notify_cb" <?php echo ($rupostupd_delivered_sms_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя по SMS</label>
            </tr>
			
            <tr>
			  <td colspan="2">Повторно уведомлять покупателя о прибытии заказа на почту через <input type="text" name="rupostupd_repeat_notify_days" value="<?php echo $rupostupd_repeat_notify_days; ?>" size="3" /> сут., если заказ не вручен покупателю.
			  </td>
			</tr>
            <tr>
              <td>Текст повторного уведомления покупателя о прибытии:</td>
              <td><textarea name="rupostupd_repeat_text" rows="6" cols="100"><?php echo $rupostupd_repeat_text; ?></textarea>
				<br/><label for="repeat_notify_cb"><input type="checkbox" name="rupostupd_repeat_notify" value="1" id="repeat_notify_cb" <?php echo ($rupostupd_repeat_notify ? ' checked="checked"' : ''); ?>/>Повторно уведомлять покупателя</label>
			  </td>
            </tr>
            <tr>
              <td>Текст повторного SMS-уведомления покупателя о прибытии:</td>
              <td><textarea name="rupostupd_repeat_sms" rows="4" cols="100"><?php echo $rupostupd_repeat_sms; ?></textarea>
				<br/><label for="repeat_sms_notify_cb"><input type="checkbox" name="rupostupd_repeat_sms_notify" value="1" id="repeat_sms_notify_cb" <?php echo ($rupostupd_repeat_sms_notify ? ' checked="checked"' : ''); ?>/>Повторно уведомлять покупателя по SMS</label>
            </tr>
			
            <tr>
              <td>Статус заказа, когда он возвращается отправителю:</td>
              <td><select name="rupostupd_returning_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_returning_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>
            <tr>
              <td>Текст уведомления покупателя о возврате:</td>
              <td><textarea name="rupostupd_returning_text" rows="6" cols="100"><?php echo $rupostupd_returning_text; ?></textarea>
				<br/><label for="delivered_returning_cb"><input type="checkbox" name="rupostupd_returning_notify" value="1" id="delivered_returning_cb" <?php echo ($rupostupd_returning_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label>
			  </td>
            </tr>
			
            <tr>
              <td>Статус заказа, когда он вручен получателю:</td>
              <td><select name="rupostupd_ok_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_ok_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>
            <tr>
              <td>Текст уведомления покупателя после вручения:</td>
              <td><textarea name="rupostupd_ok_text" rows="6" cols="100"><?php echo $rupostupd_ok_text; ?></textarea>
				<br/><label for="delivered_ok_cb"><input type="checkbox" name="rupostupd_ok_notify" value="1" id="delivered_ok_cb" <?php echo ($rupostupd_ok_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label>
			  </td>
            </tr>

            <tr>
              <td>Статус заказа, когда он вернулся в начальный пункт:</td>
              <td><select name="rupostupd_returned_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_returned_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>
			
            <tr>
              <td>Статус заказа, когда он возвращен отправителю:</td>
              <td><select name="rupostupd_fail_status">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $rupostupd_fail_status) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
			  </td>
            </tr>

            <tr>
              <td>Текст внутреннего комментария:</td>
              <td><textarea name="rupostupd_text" rows="6" cols="100"><?php echo $rupostupd_text; ?></textarea>
			  </td>
            </tr>
			
            <tr>
              <td>Подстановка данных в уведомления:</td>
              <td>
			  <span class="help">{order_id} - номер заказа, {track_no} - код отслеживания, {firstname}, {lastname} - имя и фамилия покупателя.</span>
			  <br/><span class="help">{STATUS} - статус почтового отправления; {WHERE} и {INDEX} - название и индекс отделения почтовой связи, где находится отправление.</span>
			  </td>
            </tr>
			
            <tr>
              <td>Игнорировать промежуточные статусы:<br/><span class="help">Не добавлять в историю заказа промежуточные статусы доставок.</span></td>
              <td>
			  <input type="checkbox" name="rupostupd_skip_inessential" value="1" <?php echo ($rupostupd_skip_inessential ? ' checked="checked"' : ''); ?>/>
              </td>
            </tr>
          </table>

        </div>
		<input type="hidden" name="rupostupd_set" value="1" />
      </form>
    </div>
  </div>
</div>
<script>
$('#_add_login_password').click(function() {
	var new_lp = $('.api_login_password:first').clone();
	new_lp.find('input').val('');
	$(this).before(new_lp);
})
$('#form').on('click', '._remove_login_password', function() {
	$(this).parent().remove();
})
</script>
<?php echo $footer; ?>
