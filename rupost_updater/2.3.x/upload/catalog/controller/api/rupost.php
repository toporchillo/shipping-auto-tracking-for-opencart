<?php
/**
* Russian post tracking for OpenCart (ocStore) 2.3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: http://liveopencart.ru/opencart-moduli-shablony/moduli/dostavka/avtomaticheskoe-otslejivanie-pochtovyih-otpravleniy-pochtyi-rossii
*/
//include the library
require_once(DIR_SYSTEM . 'library/russianpost.lib.php');

class ControllerApiRupost extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		//'login'=>'',
		//'password'=>'',
		'corporate'=>0,
		'assign'=>0,
		'apikey'=>'',
		'token'=>'',        
		'period'=>0.1,
		'Timezone_diff'=>4,
		'shipping_code'=>'',
		'last_hours'=>-1,
		'assign_status'=>2,
		'delivery_status'=>2,
		'returning_status'=>8,
		'delivered_status'=>3,
		'returned_status'=>13,
		'ok_status'=>5,
		'fail_status'=>10,
		'skip_inessential'=>0,
		
		'assign_text'=>"{firstname}, вашему заказу №{order_id} присвоен код почтового отправления: {track_no}.",
		'assign_notify'=>1,
		'assign_sms'=>"{firstname}, вашему заказ #{order_id} присвоен код отправления: {track_no}.",
		'assign_sms_notify'=>1,
		'start_text'=>"{firstname}, ваш заказ №{order_id} передан почте России, отделение почтовой связи '{WHERE}'. Код почтового отправления: {track_no}.",
		'start_notify'=>1,
		'start_sms'=>"{firstname}, ваш заказ #{order_id} передан почте России '{WHERE}'. Код отправления: {track_no}.",
		'start_sms_notify'=>1,
		'delivered_text'=>"{firstname}, ваш заказ №{order_id} прибыл в отделение почтовой связи '{WHERE}'. Код почтового отправления: {track_no}.",
		'delivered_notify'=>1,
		'delivered_sms'=>"{firstname}, ваш заказ #{order_id} прибыл в почтовое отделение '{WHERE}'. Код отправления: {track_no}.",
		'delivered_sms_notify'=>1,
		'repeat_text'=>"{firstname}, ваш заказ №{order_id} уже давно прибыл в отделение почтовой связи {INDEX} ({WHERE}). Код почтового отправления: {track_no}.",
		'repeat_notify'=>1,
		'repeat_sms'=>"{firstname}, ваш заказ #{order_id} уже давно прибыл в почтовое отделение {INDEX} ({WHERE}). Код отправления: {track_no}.",
		'repeat_sms_notify'=>1,
		'repeat_notify_days'=>0,
		'returning_text'=>"{firstname}, заказ №{order_id} не был вами получен в отделении почтовой связи '{WHERE}'. Почта начала возврат заказа отправителю.",
		'returning_notify'=>1,
		'ok_text'=>"{firstname}, спасибо за покупку в нашем магазине.",
		'ok_notify'=>0,
        'ok_reward'=>0,
		
		'text'=>"изменен статус почтового отправления. Новый статус: {WHERE}, {STATUS}",
		'notify'=>1,
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);

	private function setConfig() {
		if ($this->config->get('rupostupd_set')) {
			foreach($this->CONFIG as $key=>$conf) {
				$this->CONFIG[$key] = $this->config->get('rupostupd_'.$key);
			}
		}
	}

	/**
	Присвоить заказам трек-номера
	**/
	public function assign() {
		if (!$this->config->get('rupostupd_assign')) {
			return false;
		}
		$api = new OtpravkaAPI($this->config->get('rupostupd_apikey'), $this->config->get('rupostupd_token'));
		try {
			$orders = $api->getAllOrders();
			$tracks = array();
			foreach ($orders as $order) {
				$tracks[$order['order-num']] = $order['barcode'];
			}
		} catch(RussianPostException $e) {
			$this->log('Поиск доставок. Ошибка сервиса otpravka-api.pochta.ru: ' . $e->getMessage(), 0);
			return false;
		}
		foreach ($tracks as $order_id=>$track_no) {
			$this->db->query("UPDATE `".DB_PREFIX."order` SET track_no='".$this->db->escape($track_no)."' WHERE order_id='".(int)$order_id."' AND track_no=''");
			$this->log("Найден трек-номер #".$track_no." для заказа ID:".$order_id, 4);
			if ($this->db->countAffected()) {
				$this->log("Заказ ID:".$order_id." присвоен трек-номер: ".$track_no, 3);
			}
		}
	}
	
	public function update() {
		$this->setConfig();
	
		if (isset($_SERVER['HTTP_HOST'])) {
			header("Content-Type: text/html; charset=utf-8");
		}
		$this->log('Начинаем отслеживать отправления Почты России...', 3);
		if (!$this->config->get('rupostupd_status')) {
			$this->log('Модуль "Автотреккинг доставок Почты России" отключен', 3);
			return false;
		}
		
		$this->assign();

		$logins = unserialize($this->config->get('rupostupd_logins'));
		$passwords = unserialize($this->config->get('rupostupd_passwords'));
		$login_no = 0;
		$corporate = $this->config->get('rupostupd_corporate');

		$limit = $corporate ? 3000 : 100*count($logins);
        
		$orders = $this->getOrdersToUpdate($limit);
		$c = 0;
		$i = 0;
		while ($i<count($orders)) {
			$order = $orders[$i];
			$i++;
			if (!preg_match('/\w\w\d{9}\w\w/i', $order['track_no']) && !preg_match('/\d{14}/i', $order['track_no'])) {
				continue;
			}
			$c++;
			if (!$corporate && ($c % 100 == 0)) {
				$login_no++;
			}
            
			try {
				//init the client
				$this->log('Запрос к API Почты России. Login: '.$logins[$login_no], 3);
				$client = new RussianPostAPI($logins[$login_no], $passwords[$login_no]);
				//fetch info
				$state = $client->getOperationHistory($order['track_no']);
				
				$query = $this->db->query("SELECT comment, date_added FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order['order_id']."' ORDER BY date_added DESC");
				$this->handleStatus($order, $state, $query->rows);
			} catch(RussianPostException $e) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].') Ошибка сервиса Почты России: ' . $e->getMessage(), 0);
				if (strpos($e->getMessage(), 'Превышен допустимый лимит запросов') || strpos($e->getMessage(), 'Неверное имя пользователя или пароль')) {
					$c = 0;
					$login_no++;
					if ($login_no >= count($logins)) {
						$this->log('Больше нет аккаунтов для доступа к API Почты России. Обработаны не все заказы.', 3);
						break;
					}
					$i--;
				}
			}
		}
		$this->log('Отслеживание отправлений Почты России завершено.', 3);
	}
	
	protected function getOrdersToUpdate($limit) {
		$shipping_code = $this->config->get('rupostupd_shipping_code');
		$shcode_where = ($shipping_code ? " AND o.shipping_code LIKE '" . $shipping_code . "%'" : '');
		$not_in = ($this->config->get('rupostupd_order_statuses') ? $this->config->get('rupostupd_order_statuses') : '0');
		$query = $this->db->query("SELECT o.*
			FROM `" . DB_PREFIX . "order` o
			LEFT JOIN `" . DB_PREFIX . "order_history` h ON (o.order_id=h.order_id AND h.date_added>'" . date('Y-m-d H:i:s', time()-($this->CONFIG['period']*3600)) ."')
			WHERE o.track_no <> '' AND h.order_history_id IS NULL $shcode_where AND o.order_status_id <> '0' AND NOT(o.order_status_id IN($not_in)) ORDER BY h.date_added DESC LIMIT $limit");
		return $query->rows;
	}
	
	private function parseDate($str) {
		$date = date_parse($str);
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
	private function handleStatus($order, $state, $comments) {
		$this->load->model('checkout/order');
		$return = false;
		foreach ($state as $j=>$s) {
			$s = (array) $s;
			
			$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Обрабатываем данные '.json_encode($s).'.', 5);

			//Возвращается
			if ($s['operationTypeId'] == 3) {
				$return = true;
			}
			//Отсекаем старые треккинги 2-летней давности
			$date = date_parse($s['operationDate']);
			if (intval(date('Y')) - intval($date['year']) > 2) {
				$this->log('Order #'.$order['track_no'].' (ID:'.$order['order_id'].'), status is too old: ' . $s['operationDate'] . '.', 3);
				continue;
			}
			
			$already_added = false;
			foreach($comments as $i=>$row) {
				if (strpos($row['comment'], $this->parseDate($s['operationDate'])) === 0) {
					$already_added = true;
				}
			}
			$repeat_notify = false;
			if (($this->CONFIG['repeat_notify'] || $this->CONFIG['repeat_sms_notify']) && ($j == count($state)-1 || $j == (count($state)-2)) && !$return 
				&& ($s['operationTypeId'] == 8) && ($s['operationAttributeId'] == 2)
				&& ((time() - strtotime($comments[0]['date_added'])) >= 3600*24*intval($this->CONFIG['repeat_notify_days']))) {
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Повторно уведомляем покупателя о прибытии заказа.', 3);
					$already_added = false;
					$repeat_notify = true;
			}
			if ($already_added) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Данные уже обработаны ранее.', 4);
				continue;
			}
			
			$notify = false;
			 //присвоен трек-номер
			if ($s['operationTypeId'] == 28) {
				$status = $this->CONFIG['assign_status'];
				$notify = $this->CONFIG['assign_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['assign_text']);
				if ($this->CONFIG['assign_sms_notify']) {
					$msg = $this->getComment($order, $s, $this->CONFIG['assign_sms']);
					$this->smsNotify($order, $msg);
				}
            		}
			//Принята почтой
			elseif ($s['operationTypeId'] == 1) {
				$status = $this->CONFIG['delivery_status'];
				$notify = $this->CONFIG['start_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['start_text']);
				if ($this->CONFIG['start_sms_notify']) {
					$msg = $this->getComment($order, $s, $this->CONFIG['start_sms']);
					$this->smsNotify($order, $msg);
				}
			}
			 //Доставлена
			elseif (!$return && ($s['operationTypeId'] == 8) && ($s['operationAttributeId'] == 2)) {
				$status = $this->CONFIG['delivered_status'];
				if (!$repeat_notify) {
				$notify = $this->CONFIG['delivered_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['delivered_text']);
				if ($this->CONFIG['delivered_sms_notify']) {
					$msg = $this->getComment($order, $s, $this->CONFIG['delivered_sms']);
					$this->smsNotify($order, $msg);
				}
			}
				else {
					$notify = $this->CONFIG['repeat_notify'];
					$notify_text = $this->getComment($order, $s, $this->CONFIG['repeat_text']);
					if ($this->CONFIG['repeat_sms_notify']) {
						$msg = $this->getComment($order, $s, $this->CONFIG['repeat_sms']);
						$this->smsNotify($order, $msg);
					}
				}
			}
			 //Неудачная попытка вручения - Адресат заберет отправление сам
			elseif (!$return && ($s['operationTypeId'] == 12) && ($s['operationAttributeId'] == 9 || $s['operationAttributeId'] == 1)) {
				$status = $this->CONFIG['delivered_status'];
			}
			 //Возвращается
			elseif ($s['operationTypeId'] == 3) {
				$status = $this->CONFIG['returning_status'];
				$notify = $this->CONFIG['returning_notify'];
				$notify_text = $this->getComment($order, $s, $this->CONFIG['returning_text']);
			}
			 //Вручена
			elseif (!$return && $s['operationTypeId'] == 2) {
				$status = $this->CONFIG['ok_status'];
				$notify = $this->CONFIG['ok_notify'];
                if ($this->CONFIG['ok_reward']) {
                    $this->addReward($order);
                }
				$notify_text = $this->getComment($order, $s, $this->CONFIG['ok_text']);
			}
			 //Вернулась
			elseif ($return && ($s['operationTypeId'] == 8) && ($s['operationAttributeId'] == 2)) {
				$status = $this->CONFIG['returned_status'];
			}
			 //Вручен отправителю
			elseif ($return && $s['operationTypeId'] == 2) {
				$status = $this->CONFIG['fail_status'];
			}
			else {
				$status = ($return ? $this->CONFIG['returning_status'] : $this->CONFIG['delivery_status']);
				if ($this->CONFIG['skip_inessential']) {
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Пропускаем несущественный статус '.$s['operationTypeId'].'/'.$s['operationAttributeId'].': '
						.$s['operationPlaceName'].', '.$s['operationDate'].' '.$s['operationType'].($s['operationAttribute'] ? ' - '.$s['operationAttribute'] : ''), 3);
					continue;
				}
			}
		
			try {
				$date = $this->parseDate($s['operationDate']);
				$comment = $this->getComment($order, $s, $date.' '.$this->CONFIG['text']);
				$this->model_checkout_order->addOrderHistory($order['order_id'], $status, $comment, false, true);
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'), статус '.$s['operationTypeId'].'/'.$s['operationAttributeId'].'. '
					.'Добавлена история заказа: '.$comment, 3);
				if ($notify) {
					$this->model_checkout_order->addOrderHistory($order['order_id'], $status, $notify_text, true, true);
					$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Покупатель уведомлен: '.$notify_text, 3);
				}
			} catch (Exception $e) {
				$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Ошибка добавления истории заказа ('.$e->getMessage().').', 1);
			}
		}
	}

	protected function smsNotify($order, $message) {
		$options = array(
			'to'       => $order['telephone'],
			'copy'     => '',
			'from'     => $this->config->get('rupostupd_sms_gate_from'),
			'username'    => $this->config->get('rupostupd_sms_gate_username'),
			'password' => $this->config->get('rupostupd_sms_gate_password'),
			'message'  => $message,
			'ext'      => null
		);
		
		if (!class_exists('Sms')) {
			require_once(DIR_SYSTEM . 'library/sms.php');
		}

		$sms = new Sms($this->config->get('rupostupd_sms_gatename'), $options);
		$sms->send();
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 4);
	}
	
	private function getComment($order, $state, $text) {
		foreach ($order as $key=>$val) {
			$text = str_replace('{'.$key.'}', $val, $text);
		}
		$text = str_replace('{WHERE}', $state['operationPlaceName'], $text);
		$text = str_replace('{STATUS}', $state['operationType'].($state['operationAttribute'] ? ' - '.$state['operationAttribute'] : ''), $text);
		return $text;
	}

    
	private function addReward($order) {
        if (!$order['customer_id']) {
            return 0; //Некому назначать
        }
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order['order_id'] . "' AND points > 0");
        if ($query->row['total']) {
            return 0;  //Уже назначено
        }
        $reward = 0;
        $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order['order_id'] . "'");
        foreach ($order_product_query->rows as $product) {
            $reward += $product['reward'];
        }
        if (!$reward) {
            return 0; //0 баллов - не надо назначать
        }
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$order['customer_id'] . "', order_id = '" . (int)$order['order_id'] . "', points = '" . (int)$reward . "', description = 'Заказ #" . (int)$order['order_id'] . "', date_added = NOW()");
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Начисляем бонусы: '.$reward.' для покупателя ID:'.$order['customer_id'], 4);
        return $order['points'];
	}
    
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'rupost_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}
