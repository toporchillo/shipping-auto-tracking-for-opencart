<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> Редактировать настройки модуля</h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status">Статус:</label>
            <div class="col-sm-10">
              <select name="rupostupd_status" id="input-status" class="form-control">			
                <?php if ($rupostupd_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>	
		  
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="rupostupd_login">Доступ к API Почты России:</label>
            <div class="col-sm-10">
			 <?php foreach ($rupostupd_logins as $i=>$login) { ?>
			  <div class="api_login_password col-sm-12" style="margin-bottom: 5px;">
                <label class="col-sm-2 control-label"><span>Логин:</span></label>
                <div class="col-sm-3">
                    <input type="text" name="rupostupd_logins[]" value="<?php echo $login; ?>" class="form-control input-small" required />
                </div>
                <label class="col-sm-2 control-label"><span>Пароль:</span></label>
                <div class="col-sm-3">
                    <input type="password" name="rupostupd_passwords[]" value="<?php echo $rupostupd_passwords[$i]; ?>" class="form-control input-small" required />
                </div>
                <i class="fa fa-minus-circle _remove_login_password" style="cursor: pointer;"></i>
			  </div>
			<?php } ?>
              <div class="col-sm-10">
                <i class="fa fa-plus-circle _add_login_password" style="cursor: pointer;"></i>
			    <br /><a href="https://tracking.pochta.ru/specification" target="_blank">зарегистрироваться</a>
              </div>
            </div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="rupostupd_corporate"><span data-toggle="tooltip" title="Для клиентов без договора лимит доступа к серверу Почты России - 100 запросов в сутки.">Корпоративный клиент:</span></label>
			<div class="col-sm-10">
				<div class="checkbox"><label><input type="checkbox" name="rupostupd_corporate" value="1" <?php echo ($rupostupd_corporate ? ' checked="checked"' : ''); ?>/></label></div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="rupostupd_assign"><span data-toggle="tooltip" title="Если вы указали номер заказа при отправлении">Присваивать трек-номера автоматически:</span></label>
			<div class="col-sm-10">
				<div class="checkbox"><label><input type="checkbox" id="rupostupd_assign_cb" name="rupostupd_assign" value="1" <?php echo ($rupostupd_assign ? ' checked="checked"' : ''); ?>/></label></div>
			</div>
		  </div>
		  <div class="form-group otpravka_api" style="display:none;">
			<label class="col-sm-2 control-label" for="rupostupd_apikey"><span>Ключ авторизации otpravka-api.pochta.ru:</span></label>
			<div class="col-sm-10">
				<input type="text" name="rupostupd_apikey" value="<?php echo $rupostupd_apikey; ?>" class="form-control input-small" />
			</div>
		  </div>
		  <div class="form-group otpravka_api" style="display:none;">
			<label class="col-sm-2 control-label" for="rupostupd_token"><span>Токен авторизации otpravka-api.pochta.ru:</span></label>
			<div class="col-sm-10">
				<input type="text" name="rupostupd_token" value="<?php echo $rupostupd_token; ?>" class="form-control input-small" />
			</div>
		  </div>
		  
<!--
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="rupostupd_shipping_code"><span data-toggle="tooltip" title="Если вы не отправляете заказы другими способами доставки, можно оставить это поле пустым.">Код доставки Почты России в OpenCart:</span></label>
			<div class="col-sm-10">
				<input type="text" name="rupostupd_shipping_code" value="<?php echo $rupostupd_shipping_code; ?>" class="form-control input-small" />
			</div>
		  </div>
-->

		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_order_statuses">Не обрабатывать заказы с этими статусами:</label>
            <div class="col-sm-10">
              <select name="rupostupd_order_statuses[]" size="15" multiple class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"<?php echo (in_array($order_status['order_status_id'], $rupostupd_order_statuses) ? ' selected="selected"' : ''); ?>><?php echo $order_status['name']; ?></option>
				<?php } ?>
              </select>
            </div>
		  </div>
          
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_assign_status">Статус заказа, когда присвоен трек-номер:</label>
            <div class="col-sm-10">
              <select name="rupostupd_assign_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_assign_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_assign_text">Текст уведомления покупателя о том, что присвоен трек-номер:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_assign_text" rows="5" class="form-control"><?php echo $rupostupd_assign_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_assign_notify" value="1" id="assign_notify_cb" <?php echo ($rupostupd_assign_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label></div>
            </div>
            <label class="col-sm-2 control-label" for="rupostupd_assign_sms">Текст SMS-уведомления покупателя о том, что присвоен трек-номер:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_assign_sms" rows="4" class="form-control"><?php echo $rupostupd_assign_sms; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_assign_sms_notify" value="1" id="assign_sms_notify_cb" <?php echo ($rupostupd_assign_sms_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя по SMS</label></div>
            </div>
		  </div>          
		  
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_delivery_status">Статус заказа, когда он доставляется покупателю:</label>
            <div class="col-sm-10">
              <select name="rupostupd_delivery_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_delivery_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_start_text">Текст уведомления покупателя о начале доставки:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_start_text" rows="5" class="form-control"><?php echo $rupostupd_start_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_start_notify" value="1" id="start_notify_cb" <?php echo ($rupostupd_start_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label></div>
            </div>
            <label class="col-sm-2 control-label" for="rupostupd_start_sms">Текст SMS-уведомления покупателя о начале доставки:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_start_sms" rows="4" class="form-control"><?php echo $rupostupd_start_sms; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_start_sms_notify" value="1" id="start_sms_notify_cb" <?php echo ($rupostupd_start_sms_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя по SMS</label></div>
            </div>
		  </div>
			
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_delivered_status">Статус заказа, когда он прибыл в место вручения:</label>
            <div class="col-sm-10">
              <select name="rupostupd_delivered_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_delivered_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_delivered_text">Текст уведомления покупателя о прибытии:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_delivered_text" rows="5" class="form-control"><?php echo $rupostupd_delivered_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_delivered_notify" value="1" id="delivered_notify_cb" <?php echo ($rupostupd_delivered_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label></div>
            </div>
            <label class="col-sm-2 control-label" for="rupostupd_delivered_sms">Текст SMS-уведомления покупателя о прибытии:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_delivered_sms" rows="4" class="form-control"><?php echo $rupostupd_delivered_sms; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_delivered_sms_notify" value="1" id="delivered_sms_notify_cb" <?php echo ($rupostupd_delivered_sms_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя по SMS</label></div>
            </div>
		  </div>

		  <div class="form-group">
            <div class="col-sm-12">
				<label><b>Повторно уведомлять покупателя о прибытии заказа на почту через <input type="text" name="rupostupd_repeat_notify_days" value="<?php echo $rupostupd_repeat_notify_days; ?>" style="width: 40px;" class="input-small" /> сут., если заказ не вручен покупателю.</b></label>
			</div>
            <label class="col-sm-2 control-label" for="rupostupd_delivered_text">Текст повторного уведомления покупателя о прибытии:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_repeat_text" rows="5" class="form-control"><?php echo $rupostupd_repeat_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_repeat_notify" value="1" id="repeat_notify_cb" <?php echo ($rupostupd_repeat_notify ? ' checked="checked"' : ''); ?>/>Повторно уведомлять покупателя</label></div>
            </div>
            <label class="col-sm-2 control-label" for="input-cost">Текст повторного SMS-уведомления покупателя о прибытии:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_repeat_sms" rows="4" class="form-control"><?php echo $rupostupd_repeat_sms; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_repeat_sms_notify" value="1" id="repeat_sms_notify_cb" <?php echo ($rupostupd_repeat_sms_notify ? ' checked="checked"' : ''); ?>/>Повторно уведомлять покупателя по SMS</label></div>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_returning_status">Статус заказа, когда он возвращается отправителю:</label>
            <div class="col-sm-10">
              <select name="rupostupd_returning_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_returning_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_returning_text">Текст уведомления покупателя о возврате:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_returning_text" rows="5" class="form-control"><?php echo $rupostupd_returning_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_returning_notify" value="1" id="delivered_returning_cb" <?php echo ($rupostupd_returning_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label></div>
            </div>
		  </div>
			

		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_ok_status">Статус заказа, когда он вручен получателю:</label>
            <div class="col-sm-10">
              <select name="rupostupd_ok_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_ok_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>
			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_ok_text">Текст уведомления покупателя после вручения:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_ok_text" rows="5" class="form-control"><?php echo $rupostupd_ok_text; ?></textarea>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_ok_notify" value="1" id="delivered_ok_cb" <?php echo ($rupostupd_ok_notify ? ' checked="checked"' : ''); ?>/>Уведомлять покупателя</label></div>
			  <div class="checkbox"><label><input type="checkbox" name="rupostupd_ok_reward" value="1" id="delivered_ok_reward_cb" <?php echo ($rupostupd_ok_reward ? ' checked="checked"' : ''); ?>/>Начислять бонусы покупателю</label></div>
            </div>
		  </div>

			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_returned_status">Статус заказа, когда он вернулся в начальный пункт:</label>
            <div class="col-sm-10">
              <select name="rupostupd_returned_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_returned_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>			
		  </div>

			
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_fail_status">Статус заказа, когда он возвращен отправителю:</label>
            <div class="col-sm-10">
              <select name="rupostupd_fail_status" class="form-control">			
				<?php foreach ($order_statuses as $order_status) { ?>
				<?php if ($order_status['order_status_id'] == $rupostupd_fail_status) { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				<?php } ?>
				<?php } ?>
              </select>
            </div>
		  </div>

		  <div class="form-group">
            <label class="col-sm-2 control-label" for="rupostupd_text">Текст внутреннего комментария:</label>
            <div class="col-sm-10">
			  <textarea name="rupostupd_text" rows="5" class="form-control"><?php echo $rupostupd_text; ?></textarea>
            </div>
			
            <label class="col-sm-2 control-label">Подстановка данных в уведомления:</label>
            <div class="col-sm-10">
			  <span class="help">{order_id} - номер заказа, {track_no} - код отслеживания, {firstname},{lastname} - имя и фамилия покупателя.</span>
			  <br/><span class="help">{STATUS} - статус почтового отправления; {WHERE} - отделение почтовой связи, где находится отправление.</span>
            </div>
		  </div>
		  
		  
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="rupostupd_skip_inessential"><span data-toggle="tooltip" title="Не добавлять в историю заказа промежуточные статусы доставок.">Игнорировать промежуточные статусы:</span></label>
			<div class="col-sm-10">
				<div class="checkbox"><label><input type="checkbox" name="rupostupd_skip_inessential" value="1" <?php echo ($rupostupd_skip_inessential ? ' checked="checked"' : ''); ?>/></div>
			</div>
		  </div>
			
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-template">SMS шлюз:</label>
                  <div class="col-sm-10">
                    <select name="rupostupd_sms_gatename" id="select-smsgate" class="form-control">
                      <?php foreach ($sms_gatenames as $sms_gatename) { ?>
                      <?php if ($rupostupd_sms_gatename == $sms_gatename) { ?>
                      <option value="<?php echo $sms_gatename; ?>" selected="selected"><?php echo $sms_gatename; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $sms_gatename; ?>"><?php echo $sms_gatename; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                    <br />
                    <span id="sms_gate_link"></span>
				  </div>
				</div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-sms-gate-username">Логин или ID на SMS шлюзе:</label>
                  <div class="col-sm-10">
                    <input type="text" name="rupostupd_sms_gate_username" value="<?php echo $rupostupd_sms_gate_username; ?>" id="input-sms-gate-username" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-config-sms-gate-password">Пароль,token или API key на SMS шлюзе:</label>
                  <div class="col-sm-10">
                    <input type="password" name="rupostupd_sms_gate_password" value="<?php echo $rupostupd_sms_gate_password; ?>" id="input-sms-gate-password" class="form-control" />
                  </div>
				</div>	
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-config-sms-gate-from">Отправитель SMS:</label>
                  <div class="col-sm-10">
                    <input type="text" name="rupostupd_sms_gate_from" value="<?php echo $rupostupd_sms_gate_from; ?>" id="input-sms-gate-from" class="form-control" />
                  </div>
				</div>	
				
		<input type="hidden" name="rupostupd_set" value="1" />
      </form>
    </div>
  </div>
</div>
<script>
var smsGates = {
    bytehand: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
    bytehandcom: 'https://www.bytehand.com/?r=3ddf1b2421770b0c',
	smsc: 'https://smsc.ru/?ppOpenCart',
    smscru: 'https://smsc.ru/?ppOpenCart',
    smscua: 'https://smsc.ru/?ppOpenCart',
	epochtasms: 'http://www.epochta.ru/#a_aid=opencart',
	epochta: 'http://www.epochta.ru/#a_aid=opencart',
    epochtasmscomua: 'http://www.epochta.ru/#a_aid=opencart',
	unisender: 'http://www.unisender.com/?a=opencart',
    unisenderru: 'http://www.unisender.com/?a=opencart'
};
$('#select-smsgate').change(function() {
	var gate = $(this).val();
	if (smsGates[gate]) {
		$('#sms_gate_link').html('<a href="'+smsGates[gate]+'" target="_blank">Получить доступ</a>');
	}
	else {
		$('#sms_gate_link').html('');
	}
});
$('#select-smsgate').trigger('change');

$('._add_login_password').click(function() {
	var new_lp = $('.api_login_password:first').clone();
	new_lp.find('input').val('');
	$(this).parent().before(new_lp);
})

$('#form').on('click', '._remove_login_password', function() {
	$(this).parent().remove();
})

$('#rupostupd_assign_cb').click(function() {
	if ($(this).is(':checked')) {
		console.log(1);
		$('.otpravka_api').show();
	}
	else {
		$('.otpravka_api').hide();
	}		
})
<?php if ($rupostupd_assign) : ?>
$('.otpravka_api').show();
<?php endif; ?>

//$('#rupostupd_assign_cb').trigger('click');
</script>
<?php echo $footer; ?>
