<?php
/**
* Pickpoint shipping tracking for OpenCart (ocStore) 3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
* 
* Official version of this module: ...
*/

class ControllerExtensionShippingPickpointUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 3;
	public $CONFIG = array(
		'status'=>true,
		'login' => '',
		'password' => '',
		//'Timezone_diff'=>-4,
		//'shipping_code'=>'',
		//'sendcity_postcode'=>'',
        
        'assign'=>false,
        
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);
	private $last_hours = 12;
	private $last_error = '';

	public $STATUS_MAPPING = array(
	 array('name'=>'Создан', 'codes'=>array(10101),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ зарегистрирован в базе данных Pickpoint, вы можете отслеживать заказ на сайте pickpoint.ru, код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте pickpoint.ru, код отслеживания: {track_no}'),
	 array('name'=>'Выдан курьеру для доставки', 'codes'=>array(10801),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} выдан курьеру для доставки. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} выдан курьеру для доставки. Код отслеживания: {track_no}'),
	 array('name'=>'Доставлен до постамата', 'codes'=>array(10901),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} доставлен до постамата. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} ждет вас в постамате. Код отслеживания: {track_no}'),
	 array('name'=>'Доставлен до пункта выдачи', 'codes'=>array(10902),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} доставлен до пункта выдачи. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} ждет вас в пункте выдачи. Код отслеживания: {track_no}'),
	 array('name'=>'Вручен', 'codes'=>array(11003, 11004, 11101, ),
		'notify'=>true, 'enabled'=>true, 'status'=>5, 'text'=>'Спасибо за заказ. Приходите к нам ещё!',
		'smsnotify'=>true, 'sms'=>'{firstname}, спасибо за заказ. Приходите к нам ещё!'),
	 array('name'=>'Не вручен, возврат', 'codes'=>array(11401, 11402, 11501),
		'notify'=>false, 'enabled'=>true, 'status'=>8, 'text'=>'Покупатель отказался от покупки, возврат в ИМ')
	);

	private function setConfig() {
		if (!$this->config->get('pickpointupd_set')) {
			foreach($this->STATUS_MAPPING as $code=>$vals) {
				$this->STATUS_MAPPING[$code]['text'] = $vals['text'];
			}
			return;
		}
	
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('pickpointupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('shipping_pickpoint_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('pickpointupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('pickpointupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('pickpointupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('pickpointupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('pickpointupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('pickpointupd_mapping'.$code.'_smsnotify');
			}
		}
    }
	
	public function install() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` LIMIT 1");
		if (!isset($query->row['track_no'])) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD  `track_no` VARCHAR(32) NOT NULL AFTER `order_id`");
		}
	}

	public function uninstall() {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code`='pickpointupd'");
	}
	
	public function index() {
		$this->load->language('extension/shipping/pickpoint_updater');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('pickpointupd', $this->request->post);	
			$module_status = array('shipping_pickpoint_updater_status'=>$this->request->post['pickpointupd_status']);
			$this->model_setting_setting->editSetting('shipping_pickpoint_updater', $module_status);	
            
			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'));
			return;
		}
        
		$data = array();
		$this->setConfig();
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['cron_run'] = 'php '.realpath(DIR_CATALOG.'../pickpoint_updater.php');
		$data['wget_run'] = 'wget -O - '.HTTPS_CATALOG.'index.php?route=api/pickpoint_updater/update';

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/shipping/pickpoint_updater', 'user_token=' . $this->session->data['user_token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('extension/shipping/pickpoint_updater', 'user_token=' . $this->session->data['user_token'], 'SSL');
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', 'SSL'); 		
		
		
		$this->load->model('localisation/order_status');
    	$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		foreach($this->CONFIG as $key=>$conf) {
			if (isset($this->request->post['pickpointupd_'.$key])) {
				$data['pickpointupd_'.$key] = $this->request->post['pickpointupd_'.$key];
			} else {
				$data['pickpointupd_'.$key] = $conf;
			}
		}
		
		$data['status_mapping'] = $this->STATUS_MAPPING;

		if (isset($this->request->post['pickpoint_updater_status'])) {
			$data['pickpoint_updater_status'] = $this->request->post['pickpointupd_status'];
		} else {
			$data['pickpoint_updater_status'] = $this->CONFIG['status'];
		}
		
		foreach($this->STATUS_MAPPING as $code=>$val) {
			if (isset($this->request->post['pickpointupd_mapping'.$code.'_notify'])) {
				$data['pickpointupd_mapping'.$code.'_notify'] = $this->request->post['pickpointupd_mapping'.$code.'_notify'];
			} else {
				$data['pickpointupd_mapping'.$code.'_notify'] = $val['notify'];
			}
			if (isset($this->request->post['pickpointupd_mapping'.$code.'_smsnotify'])) {
				$data['pickpointupd_mapping'.$code.'_smsnotify'] = $this->request->post['pickpointupd_mapping'.$code.'_smsnotify'];
			} elseif (isset($val['smsnotify'])) {
				$data['pickpointupd_mapping'.$code.'_smsnotify'] = $val['smsnotify'];
			}
			
			if (isset($this->request->post['pickpointupd_mapping'.$code.'_enabled'])) {
				$data['pickpointupd_mapping'.$code.'_enabled'] = $this->request->post['pickpointupd_mapping'.$code.'_enabled'];
			} else {
				$data['pickpointupd_mapping'.$code.'_enabled'] = $val['enabled'];
			}
			
			if (isset($this->request->post['pickpointupd_mapping'.$code.'_status'])) {
				$data['pickpointupd_mapping'.$code.'_status'] = $this->request->post['pickpointupd_mapping'.$code.'_status'];
			} else {
				$data['pickpointupd_mapping'.$code.'_status'] = $val['status'];
			}
			
			if (isset($this->request->post['pickpointupd_mapping'.$code.'_text'])) {
				$data['pickpointupd_mapping'.$code.'_text'] = $this->request->post['pickpointupd_mapping'.$code.'_text'];
			} else {
				$data['pickpointupd_mapping'.$code.'_text'] = $val['text'];
			}

			if (isset($this->request->post['pickpointupd_mapping'.$code.'_sms'])) {
				$data['pickpointupd_mapping'.$code.'_sms'] = $this->request->post['pickpointupd_mapping'.$code.'_sms'];
			} elseif (isset($val['sms'])) {
				$data['pickpointupd_mapping'.$code.'_sms'] = $val['sms'];
			}
		}
		
		$data['sms_gatenames'] = array();
		$files = glob(DIR_SYSTEM . 'smsgate/*.php');
		foreach ($files as $file) {
			$data['sms_gatenames'][] =  basename($file, '.php');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/shipping/pickpoint_updater', $data));		
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/pickpoint_updater')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
