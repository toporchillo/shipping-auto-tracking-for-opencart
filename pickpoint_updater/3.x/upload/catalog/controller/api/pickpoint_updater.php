<?php
/**
* Pickpoint shipping tracking for OpenCart (ocStore) 3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: ...
*/

class ControllerApiPickpointUpdater extends Controller {
	private $API_URL = 'https://e-solution.pickpoint.ru/api';
	public $ECHO = true;
	public $LOG_LEVEL = 4;
	public $CONFIG = array(
		'status'=>true,
		'login' => '',
		'password' => '',
		//'Timezone_diff'=>-4,
		'shipping_code'=>'',
        
		'assign'=>false,
		
		'sms_gatename'=>'',
		'sms_gate_username'=>'',
		'sms_gate_password'=>'',
		'sms_gate_from'=>''
	);
	private $last_hours = 12;
	private $last_error = '';

	public $STATUS_MAPPING = array(
	 array('name'=>'Создан', 'codes'=>array(10101),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ зарегистрирован в базе данных Pickpoint, вы можете отслеживать заказ на сайте pickpoint.ru, код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} отправлен. Вы можете отслеживать заказ на сайте pickpoint.ru, код отслеживания: {track_no}'),
	 array('name'=>'Выдан курьеру для доставки', 'codes'=>array(10801),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} выдан курьеру для доставки. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} выдан курьеру для доставки. Код отслеживания: {track_no}'),
	 array('name'=>'Доставлен до постамата', 'codes'=>array(10901),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} доставлен до постамата. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} ждет вас в постамате. Код отслеживания: {track_no}'),
	 array('name'=>'Доставлен до пункта выдачи', 'codes'=>array(10902),
		'notify'=>true, 'enabled'=>true, 'status'=>15, 'text'=>'Заказ #{order_id} доставлен до пункта выдачи. Код отслеживания: {track_no}',
		'smsnotify'=>true, 'sms'=>'{firstname}, ваш заказ #{order_id} ждет вас в пункте выдачи. Код отслеживания: {track_no}'),
	 array('name'=>'Вручен', 'codes'=>array(11003, 11004, 11101, ),
		'notify'=>true, 'enabled'=>true, 'status'=>5, 'text'=>'Спасибо за заказ. Приходите к нам ещё!',
		'smsnotify'=>true, 'sms'=>'{firstname}, спасибо за заказ. Приходите к нам ещё!'),
	 array('name'=>'Не вручен, возврат', 'codes'=>array(11401, 11402, 11501),
		'notify'=>false, 'enabled'=>true, 'status'=>8, 'text'=>'Покупатель отказался от покупки, возврат в ИМ')
	);
    
	private function setConfig() {
		foreach($this->CONFIG as $key=>$conf) {
			$this->CONFIG[$key] = $this->config->get('pickpointupd_'.$key);
		}
		$this->CONFIG['status']	= $this->config->get('shipping_pickpoint_updater_status');
		foreach($this->STATUS_MAPPING as $code=>$vals) {
			$this->STATUS_MAPPING[$code]['notify'] = $this->config->get('pickpointupd_mapping'.$code.'_notify');
			$this->STATUS_MAPPING[$code]['enabled'] = $this->config->get('pickpointupd_mapping'.$code.'_enabled');
			$this->STATUS_MAPPING[$code]['status'] = $this->config->get('pickpointupd_mapping'.$code.'_status');
			$this->STATUS_MAPPING[$code]['text'] = $this->config->get('pickpointupd_mapping'.$code.'_text');
			if (isset($this->STATUS_MAPPING[$code]['sms'])) {
				$this->STATUS_MAPPING[$code]['sms'] = $this->config->get('pickpointupd_mapping'.$code.'_sms');
			}
			if (isset($this->STATUS_MAPPING[$code]['smsnotify'])) {
				$this->STATUS_MAPPING[$code]['smsnotify'] = $this->config->get('pickpointupd_mapping'.$code.'_smsnotify');
			}
		}
	}

	/**
	Присвоить заказу трек-номер
	**/
	private function assign($order_id, $track_no) {
		if (!$this->CONFIG['assign']) {
			return false;
		}
		$this->db->query("UPDATE `".DB_PREFIX."order` SET track_no='".$this->db->escape($track_no)."' WHERE order_id='".(int)$order_id."' AND track_no='' LIMIT 1");
		$this->log("Найден трек-номер #".$track_no." для заказа ID:".$order_id, 4);
		if ($this->db->countAffected()) {
			$this->log("Заказ ID:".$order_id." присвоен трек-номер: ".$track_no, 3);
		}
	}
	
	public function update() {
		$this->setConfig();
    	$this->language->load('sale/order');
	
		if (isset($_SERVER['HTTP_HOST'])) {
			header("Content-Type: text/html; charset=utf-8");
		}
		$this->log('Pickpoint-tracking Started', 3);
		if (!$this->config->get('shipping_pickpoint_updater_status')) {
			$this->log('Модуль "Автотреккинг доставок Pickpoint" отключен', 3);
			return false;
		}
		
        
		//$from_date = date('Y-m-d\TH:i:s', time() + $this->CONFIG['Timezone_diff']*3600 - $this->last_hours*3600);
		if ($this->loginPickpoint()) {
			$tuData = $this->getStatuses();
			if ($tuData !== false && is_array($tuData)) {
				foreach ($tuData as $state) {
					$this->handleStatus($state);
				}
			}
		}
        
		$this->log('Pickpoint-tracking Finished', 3);
	}
	
	private function parseDate($str) {
		$date = date_parse($str);
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
    
    /*
	private function addReward($order) {
        if (!$order['customer_id']) {
            return 0; //Некому назначать
        }
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order['order_id'] . "' AND points > 0");
        if ($query->row['total']) {
            return 0;  //Уже назначено
        }
        $reward = 0;
        $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order['order_id'] . "'");
        foreach ($order_product_query->rows as $product) {
            $reward += $product['reward'];
        }
        if (!$reward) {
            return 0; //0 баллов - не надо назначать
        }
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$order['customer_id'] . "', order_id = '" . (int)$order['order_id'] . "', points = '" . (int)$reward . "', description = 'Заказ #" . (int)$order['order_id'] . "', date_added = NOW()");
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Начисляем бонусы: '.$reward.' для покупателя ID:'.$order['customer_id'], 4);
        return $order['points'];
	}
    */
    
    //============
    
	private $SessionId = '';
	
	private function loginPickpoint() {
		$request = json_encode(array(
			'Login'=>$this->CONFIG['login'],
			'Password'=>$this->CONFIG['password']
		));
		$auth = $this->postRequest('/login', 'POST', $request);
		if ($auth) {
			if (isset($auth['ErrorMessage'])) {
				$this->log('Pickpoint API ошибка авторизации: ' . $auth['ErrorMessage'], 1);
				return false;
			}
			$this->SessionId = $auth['SessionId'];
			return true;
		}
		return false;
	}
	
	private function getStatuses() {
		$date_from = date('d.m.Y H:i', time() - $this->last_hours*3600);
		$date_to = date('d.m.Y H:i', time());
		
		$request = json_encode(array(
			'SessionId'=>$this->SessionId,
			'DateFrom'=>$date_from,
			'DateTo'=>$date_to,
		));
		return $this->postRequest('/getInvoicesChangeState', 'POST', $request);
	}
	
	/**
	* Отправка POST-запроса серверу Pickpoint
	**/
	private function postRequest($url, $method, $request) {
		$url = $this->API_URL.$url;
		$this->log('Sending request to '.$url.":\n" . $request, 5);
		
		$tuCurl = curl_init();		
		curl_setopt($tuCurl, CURLOPT_URL, $url);
		curl_setopt($tuCurl, CURLOPT_HEADER, 0);
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($tuCurl, CURLOPT_POST, 1);
		curl_setopt($tuCurl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($tuCurl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json'
		));
		$tuData = curl_exec($tuCurl);
		if(curl_errno($tuCurl)){
			$info = curl_getinfo($tuCurl);
			$this->log('Curl Error: '.curl_error($tuCurl).'. Took: ' . $info['total_time'] . 'sec. URL: ' . $info['url'], 1);
			curl_close($tuCurl);		
			return false;
		} else {
			curl_close($tuCurl);
			try {
				$data = json_decode($tuData, true);
				return $data;
			}
			catch (Exception $e) {
				$this->log('Pickpoint API returns non-JSON response: ' . $tuData, 1);
				return false;
			}
		}
	}
	
	private function handleStatus($state) {
		$track_no = $state['InvoiceNumber'];
		
		$this->assign($state['SenderInvoiceNumber'], $track_no);

		$order_id = $this->findOrder($track_no);
		if (!$order_id) {
			$this->log('Order #'.$track_no.', status: '.$state['State'].'. Order not found.', 1);
			return false;
		}

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);
		$new_status = $order_info['order_status_id'];
		
		$triggered = false;
		foreach($this->STATUS_MAPPING as $mapping_item) {
			if ($mapping_item['enabled'] && in_array($state['VisualStateCode'], $mapping_item['codes'])) {
				$new_status = $mapping_item['status'];
				$triggered = $mapping_item;
				break;
			}
		}
		
		try {
			$comment = $state['ChangeDT'].' Отслеживание Pickpoint отправления '.$state['InvoiceNumber'].': '.$state['VisualState'].' ('.$state['VisualStateCode'].')';
			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $new_status, $comment, false, true);
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['VisualStateCode'].' ('.$state['VisualState'].'). Added order history.', 4);
		} catch (Exception $e) {
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['VisualStateCode'].' ('.$status.'). Unable to add order history ('.$e->getMessage().').', 1);
		}

		if (!$triggered) {
			return false;
		}
		$comment = $this->getComment($state, $order_info, $triggered);
		/*
		$q1 = $this->db->query("SELECT * FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order_id."' ORDER BY date_added DESC LIMIT 1");
		if ($q1->row && $q1->row['order_status_id'] == $new_status && strpos($q1->row['comment'], $comment) !== false) {
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['VisualStateCode'].' ('.$state['VisualState'].'). Status already changed.', 3);
			return false;
		}
		*/
		
		try {
			$this->model_checkout_order->addOrderHistory($order_id, $triggered['status'], $comment, $triggered['notify'], true);
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['VisualStateCode'].' ('.$state['VisualState'].'). Added order history.', 3);
			if (isset($triggered['smsnotify']) && $triggered['smsnotify']) {
				$this->smsNotify($order_info, $this->getComment($state, $order_info, $triggered, 'sms'));
			}
			return true;
		} catch (Exception $e) {
			$this->log('Order #'.$track_no.' (ID:'.$order_id.'), status: '.$state['VisualStateCode'].' ('.$state['VisualState'].'). Unable to add order history ('.$e->getMessage().').', 1);
			return false;
		}	
	}

	/**
	* Получить ID заказа по номеру треккинга
	*/
	private function findOrder($track_no) {
		if ($this->CONFIG['shipping_code'] == '') {
			$sql = "SELECT order_id FROM `".DB_PREFIX."order` WHERE track_no='".$this->db->escape(trim($track_no))."' LIMIT 1";
		}
		else {
			$sql = "SELECT order_id FROM `".DB_PREFIX."order` WHERE track_no='".$this->db->escape(trim($track_no))."' 
				AND shipping_code='".$this->db->escape($this->CONFIG['shipping_code'])."' LIMIT 1";
		}
		
		$q = $this->db->query($sql);
		if (!$q->row) {
			return false;
		}
		else {
			return $q->row['order_id'];
		}
	}

	private function getComment($state, $order_info, $triggered, $key='text') {
		$date = $state['ChangeDT'];
		$text = str_replace('{DATE}', $date, $triggered[$key]);
		$text = str_replace('{firstname}', $order_info['firstname'], $text);
		$text = str_replace('{lastname}', $order_info['lastname'], $text);
		$text = str_replace('{order_id}', $order_info['order_id'], $text);
		$text = str_replace('{track_no}', $order_info['track_no'], $text);
		$address_arr = array($order_info['shipping_zone'], $order_info['shipping_city'], $order_info['shipping_address_1'], $order_info['shipping_address_2']);
		$address_arr = array_diff($address_arr, array(''));
		$text = str_replace('{address}', implode(', ', $address_arr), $text);
		return $text;
	}

	private function smsNotify($order, $message) {
		$options = array(
			'to'       => $order['telephone'],
			'copy'     => '',
			'from'     => $this->CONFIG['sms_gate_from'],
			'username'    => $this->CONFIG['sms_gate_username'],
			'password' => $this->CONFIG['sms_gate_password'],
			'message'  => $message,
			'ext'      => null
		);
		
		if (!class_exists('Sms')) {
			require_once(DIR_SYSTEM . 'library/sms.php');
		}

		$sms = new Sms($this->config->get('pickpointupd_sms_gatename'), $options);
		$sms->send();
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 4);
	}
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'pickpoint_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}
