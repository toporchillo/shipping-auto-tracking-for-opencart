<?php
/**
* Grastin shipping tracking for OpenCart (ocStore) 2.3.x
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2014- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Official version of this module: ...
*/
class ControllerApiGrastinUpdater extends Controller {
	public $ECHO = true;
	public $LOG_LEVEL = 3;
	public $CONFIG = array(
		'status'=>true, //Вкл-выкл
		'api_key' => '6b29f368-f6b7-4f75-a4db-81007f5612c1', //API key Grastin
		'order_prefix' => 'ЧДЛ',
		'shipping_codes'=>array('ll_grastin.ll_grastin_pickup_grastin', 'll_grastin.ll_grastin_pickup_partner'), //Обрабатывать заказы тлько с этими shipping_code
		'oc_shipped_status'=>33, //Устанавливать такой статус заказа, когда он готов к выдаче
		'oc_complete_status'=>25, //Устанавливать такой статус заказа, когда он готов к выдаче
		'oc_return_status'=>38, //Устанавливать такой статус заказа, когда возврат
		'ignore_order_statuses'=>array(0, 25, 36, 37, 40), //Не отслеживать заказы со этими order_status_id
		//Текст комментария
		'notification'=>"Здравствуйте, {firstname}!
Ваш заказ №{order_id} доставлен в пункт выдачи по адресу: {pickup_address}.
Магазин Для-колясок.рф"
	);
	private $last_error = '';

	public function update() {
		if ($this->ECHO) {
			header('Content-Type: text/html; charset=utf-8');
		}
		$this->load->model('checkout/order');
		if (!$this->CONFIG['status']) return false;

		$orders = $this->getOrdersToUpdate();
		if (count($orders) > 0) {
			$this->handleStatuses($orders);
		}
		else {
			$this->log('No Grastin orders to update.', 1);
		}
	}
	
	protected function getOrdersToUpdate() {
		$shcode_where = ($this->shipping_codes ? " AND o.shipping_code IN ('" . implode("','", $shipping_code) . "')" : '');
		$not_in =  implode(",",$this->CONFIG['ignore_order_statuses']);
		$query = $this->db->query("SELECT o.* FROM `" . DB_PREFIX . "order` o
			WHERE NOT(o.order_status_id IN($not_in)) $shcode_where AND order_id=10016187 ORDER BY order_id DESC LIMIT 100");
		return $query->rows;
	}

	/**
	Узнать статусы заказов и обработать их по одному
	**/
	private function handleStatuses($orders) {
		$request = '<?xml version="1.0" encoding="UTF-8"?>
<File>
<API>'.$this->CONFIG['api_key'].'</API>
<Method>statushistory</Method>
<Orders>
';
		foreach($orders as $order) {
			$track_no = $this->CONFIG['order_prefix'].$order['order_id'];
			$request.= '<Order>'.$track_no.'</Order>'."\n";
		}
		$request.= '</Orders>
</File>';
		$xml = $this->postRequest($request);
		
		if (!$xml) {
			$this->log('Grastin returns empty XML', 1);
			return false;
		}
		
		try {
			$resp = simplexml_load_string($xml);
		}
		catch (Exception $e) {
			$this->log('Grastin returns invalid XML: '.$xml, 1);
			return false;
		}
		
		if (isset($resp->Error)) {
			$this->log('Grastin returns Error: '.$resp->Error[0], 1);
			return false;
		}
		if (isset($resp->Order)) {
			$states = array();
			foreach ($resp->Order as $grastinOrder) {
				$gOrder = (array) $grastinOrder[0];
				$states[(string) $gOrder['Number']] = $gOrder['Record'];
			}
			foreach ($orders as $order) {
				if (isset($states[$this->CONFIG['order_prefix'].$order['order_id']])) {
					$this->handleStatus($order, $states[$this->CONFIG['order_prefix'].$order['order_id']]);
				}
			}
			return true;
		}
		
		$this->log('Grastin returns unexpected XML: '.$xml, 1);
		return false;
	}
	
	/**
	* Отправка POST-запроса серверу Grastin
	**/
	private function postRequest($xml) {
        $url = 'http://api.grastin.ru/api.php';
		$this->log('Sending request to '.$url.":\n" . $xml, 5);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, 'XMLPackage='.urlencode($xml));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $tuData = curl_exec($ch);
		if(curl_errno($ch)){
			$info = curl_getinfo($ch);
			$this->log('Curl Error: '.curl_error($ch).'. Took: ' . $info['total_time'] . 'sec. URL: ' . $info['url'], 1);
			curl_close($ch);		
			return false;
		} else {
			curl_close($ch);		
			return $tuData;
		}
	}
	
	private function handleStatus($order, $state) {
		$this->load->model('checkout/order');
		foreach ($state as $s) {
			$status = $s->Status[0];
			$statusDate = $s->StatusDate[0];
			
			$this->log('Order ID:'.$order['order_id'].'. Handle tracking '.$statusDate.' - '.$status.'.', 5);
			
			if ($status == 'shipping' || $status == 'done' || $status == 'return') {
				$tech_comment = $statusDate . ' Доставка в статусе '.$status; //Тенический комментарий, нужен, чтобы не уведомлять по второму разу
				
				$q1 = $this->db->query("SELECT * FROM `".DB_PREFIX."order_history` WHERE order_id='".(int)$order['order_id']."' AND comment LIKE '".$this->db->escape($tech_comment)."%' ORDER BY date_added DESC LIMIT 1");
				if ($q1->row) {
					$this->log('Order ID:'.$order['order_id'].', status: '.$status.', already changed.', 3);
					continue;
				}
				
				if ($status == 'shipping') {
					$order_status_id = $this->CONFIG['oc_shipped_status'];
				}
				elseif ($status == 'done') {
					$order_status_id = $this->CONFIG['oc_complete_status'];
				}
				else {
					$order_status_id = $this->CONFIG['oc_return_status'];
				}
				
				$this->model_checkout_order->addOrderHistory($order['order_id'], $order_status_id, $tech_comment, false, true);
				$this->log('Order ID:'.$order['order_id'].', status: '.$status.'. Added order history: '.$tech_comment, 3);
				
				if ($status == 'shipping') {
					$comment = $this->getComment($order, $this->CONFIG['notification']);
					$this->model_checkout_order->addOrderHistory($order['order_id'], $order_status_id, $comment, true, true);
					
					$this->log('Order ID:'.$order['order_id'].', status: '.$status.'. Added order history. User notified: '.$comment, 3);
					//SMS?
				}
			}
		}
	}

	private function getComment($order, $text) {
		foreach ($order as $key=>$val) {
			$text = str_replace('{'.$key.'}', $val, $text);
		}
		$pickup_address = trim(str_replace('Grastin', '', strip_tags($order['shipping_method'])));
		$text = str_replace('{pickup_address}', $pickup_address, $text);
		$text = str_replace('{STATUS}', $state['operationType'].($state['operationAttribute'] ? ' - '.$state['operationAttribute'] : ''), $text);
		return $text;
	}

	private function parseDate($str) {
		$date = date_parse(str_replace('T', ' ', $str));
		return date('d.m.Y H:i:s', mktime( $date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']));
	}
	
	/*
	protected function smsNotify($order, $message) {
		$options = array(
			'to'       => $order['telephone'],
			'copy'     => '',
			'from'     => $this->CONFIG['sms_gate_from'],
			'username'    => $this->CONFIG['sms_gate_username'],
			'password' => $this->CONFIG['sms_gate_password'],
			'message'  => $message,
			'ext'      => null
		);
		
		if (!class_exists('Sms')) {
			require_once(DIR_SYSTEM . 'library/sms.php');
		}

		$sms = new Sms($this->config->get('boxberryupd_sms_gatename'), $options);
		$sms->send();
		$this->log('Заказ #'.$order['track_no'].' (ID:'.$order['order_id'].'). Отправляем SMS, тел.: ' . $order['telephone'] . ' (' . $message . ').', 4);
	}
	*/
	
	/**
	* Писать в журнал ошибки и сообщения
	* @param str $msg запись
	* @param int $level приоритет ошибки/сообщения. Если приоритет больше $this->LOG_LEVEL, то он записан не будет
	**/
	private function log($msg, $level = 0) {
		if ($level > $this->LOG_LEVEL) return;
		$fp = fopen(DIR_LOGS.'grastin_updater.log', 'a');
		fwrite($fp, date('Y-m-d H:i:s').': '.str_replace("\n", '', $msg)."\n");
		if ($this->ECHO) echo nl2br(htmlspecialchars($msg))."<br/>\n";
		fclose($fp);
	}
}
